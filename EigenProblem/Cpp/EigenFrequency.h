#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/utilities.h>
#include <deal.II/base/index_set.h>
#include <deal.II/base/timer.h>

#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/affine_constraints.h>
#include <deal.II/lac/sparsity_tools.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>

#include <deal.II/grid/grid_in.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/grid_generator.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_q.h>

#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/error_estimator.h>

#include <deal.II/distributed/shared_tria.h>
#include <deal.II/grid/grid_refinement.h>

#include <deal.II/lac/petsc_sparse_matrix.h>
#include <deal.II/lac/petsc_vector.h>
#include <deal.II/lac/slepc_solver.h>

#include <fstream>

// Import the params class, from the PT-equations module.
#include "../../PT_Eqns/Cpp/Parameters.cpp"

using namespace dealii;
using std::endl;

// ██████████████████████████████████████████████████████████████████████████ //

template <int dim>
class EigenFrequency
{
public:
   EigenFrequency();
   ~EigenFrequency();
   void run();

private:
   void import_grid();
   void setup_system();
   void assemble_system();
   void solve();
   void export_solution();
   void refine_grid();

   // Recursive function to export the adaptivity of a cell's child.
   void child_adaptivity(const typename DoFHandler<dim> ::cell_iterator &cell,
                         std::ofstream &output);

   parallel::shared::Triangulation<dim> triangulation;
   FESystem<dim> fe;
   DoFHandler<dim> dof_handler;

   Parameters parameters;           // Constants for the model.

   // Global matrices and constraint matrices
   AffineConstraints<PetscScalar> hanging_nodes;
   PETScWrappers::MPI::SparseMatrix A, M;

   // Eigen-functions. Can be output for visualization purposes.
   std::vector<Vector<double>> eigenvectors;

   // Index sets that contain DoFs owned by a processor, and locally active DoFs
   IndexSet locally_relevant_dofs, locally_owned_dofs;
   
   // Current processor rank, number of processors
   const unsigned int rank, world_size;

   // Order of the FE polynomial space
   static const unsigned int FE_order = 2;

   // Number of eigenvectors to extract
   PetscInt n_eigs = 5;

   // Geometrical mesh labels
   const unsigned int QTF_bottom_id = 1,
                      QTF_symmetrical_face = 2,
                      QTF_curved_boundary = 3;

   ConditionalOStream pcout;        // Displays output *only* from rank 0
   TimerOutput timer;               // Wall clock time
};

#include "EigenFrequency.cpp"
