// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
EigenFrequency<dim> :: ~EigenFrequency()
{
   dof_handler.clear();
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
EigenFrequency<dim> :: EigenFrequency() :
   triangulation(MPI_COMM_WORLD,
                 // Additional options for mesh refinement.
                 typename Triangulation<dim>::MeshSmoothing
                 (Triangulation<dim>::smoothing_on_refinement |
                  Triangulation<dim>::limit_level_difference_at_vertices)),
   fe(FE_Q<dim> (FE_order), dim),
   dof_handler(triangulation),
   rank(Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)),
   world_size(Utilities::MPI::n_mpi_processes(MPI_COMM_WORLD)),
   pcout(std::cout, rank == 0),
   timer(MPI_COMM_WORLD, pcout, TimerOutput::summary, TimerOutput::wall_times)
{
   // Set up additional non-primitive constants.
   parameters.setup();

   pcout << "Laser center: " << parameters.laser_center << endl;
   pcout << "Relative height: " << parameters.height_z_index << "/20" << endl;
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void EigenFrequency<dim> :: import_grid()
{
   // Import full mesh that includes both the gas and the QTF domains,
   Triangulation<dim> air_and_qtf_mesh;
   GridIn<dim> import_mesh;
   import_mesh.attach_triangulation(air_and_qtf_mesh);
   std::ifstream mesh_input ("../../PT_Eqns/mesh.msh");
   import_mesh.read_msh(mesh_input);
   
   // and extract the QTF submesh.
   const unsigned int tf_id = 1;
   std::set<typename Triangulation<dim>::active_cell_iterator> cells_to_remove;
   for (const auto &cell: air_and_qtf_mesh.active_cell_iterators())
      if (cell -> material_id() != tf_id)
         cells_to_remove.insert(cell);
   GridGenerator::create_triangulation_with_removed_cells(air_and_qtf_mesh,
                                                          cells_to_remove,
                                                          triangulation);

   const double g = parameters.g;
   const double cylinder_middle_x = 2*parameters.w + g,
                cylinder_middle_y = parameters.hp + g/2;

   // Due to the extraction, all boundary indicators are now lost. So we also
   // need to label boundaries (bottom, y=0 face and the curved semicylinder).
   for (const auto &cell: dof_handler.active_cell_iterators())
   {
      for (unsigned int face_id = 0;
           face_id < GeometryInfo<dim>::faces_per_cell; ++face_id)
      {
         if (cell -> face(face_id) -> at_boundary())
         {
            const Point<dim> face_center = cell -> face(face_id) -> center();
            if (std::abs(face_center[2]) < 1.0e-14)
               cell -> face(face_id) -> set_boundary_id(QTF_bottom_id);
            else if (face_center[1] < 1.0e-14)
               cell -> face(face_id) -> set_boundary_id(QTF_symmetrical_face);
            else
            {
               const double dist_sq = pow(face_center[0]-cylinder_middle_x, 2) +
                                      pow(face_center[1]-cylinder_middle_y, 2);
               if (std::abs(dist_sq - g*g/4) < 1.0e-14)
                  cell -> face(face_id) -> set_boundary_id(QTF_curved_boundary);
            }
         }
      }
   }

   // Ensure that the semi-cylinder at the bottom of the gap retains its
   // shape after refinement. Currently not implemented in other modules, so
   // not used here. This does however significantly improve the eigen-frequency
   // estimate, so should be implemented if possible.
/*
   static const CylinderBoundary<dim> cylinder (g/2,
                   Point<dim> (0, 1, 0),
                   Point<dim> (2*parameters.w + g, 0, parameters.hp + g/2));
   for (const auto &cell : dof_handler.active_cell_iterators())
      for (unsigned int f = 0; f < GeometryInfo<dim>::faces_per_cell; ++f)
         if (cell -> face(f) -> boundary_id() == QTF_curved_boundary)
            cell -> face(f) -> set_all_boundary_ids(QTF_curved_boundary);
   triangulation.set_manifold(QTF_curved_boundary, cylinder);
*/

   // Refine near the laser. 
   for (const auto &cell: dof_handler.active_cell_iterators())
   {
      const Tensor<1, dim> pt = cell->center() - parameters.laser_center;
      
      if (pt[2]*pt[2] + pt[0]*pt[0] < pow(g/2. + 1.5e-4, 2))
         cell -> set_refine_flag();
   }
   triangulation.execute_coarsening_and_refinement();

   // Number of refinements to perform
   PetscInt n_global_ref = 0;
   CHKERRXX(PetscOptionsGetInt(NULL, NULL, "-nGlobalRefinements",
            &n_global_ref, NULL));
   triangulation.refine_global(n_global_ref);
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void EigenFrequency<dim> :: setup_system()
{
   // Allocate and determine the index sets of locally owned/relevant DoFs.
   dof_handler.distribute_dofs(fe);
   pcout << "# of DoFs: " << dof_handler.n_dofs() << endl;

   locally_owned_dofs = dof_handler.locally_owned_dofs();
   DoFTools::extract_locally_relevant_dofs(dof_handler, locally_relevant_dofs);

   // Set up hanging node constraints and boundary conditions.
   hanging_nodes.clear();
   DoFTools::make_hanging_node_constraints(dof_handler, hanging_nodes);

   // Set the bottom of the QTF to be stationary.
   DoFTools::make_zero_boundary_constraints(dof_handler,
                                            QTF_bottom_id,
                                            hanging_nodes);

   // We are modeling only half of the tuning fork (y>0), since the solution is
   // symmetric across the y=0 plane for the proper mode of vibration. Thus
   // there will be no motion across this plane of symmetry, and we can impose
   // this condition explicity.
   const std::vector<bool> select_Y = {false, true, false};
   DoFTools::make_zero_boundary_constraints(dof_handler,
                                            QTF_symmetrical_face,
                                            hanging_nodes,
                                            fe.component_mask(select_Y));
   hanging_nodes.close();

  // Sparsity pattern, matrix initialization.
   DynamicSparsityPattern dsp (locally_relevant_dofs);
   DoFTools::make_sparsity_pattern(dof_handler, dsp, hanging_nodes, false);
   SparsityTools::distribute_sparsity_pattern(dsp,
                                              locally_owned_dofs,
                                              MPI_COMM_WORLD,
                                              locally_relevant_dofs);
   A.reinit(locally_owned_dofs, locally_owned_dofs, dsp, MPI_COMM_WORLD);
   M.reinit(locally_owned_dofs, locally_owned_dofs, dsp, MPI_COMM_WORLD);
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void EigenFrequency<dim> :: assemble_system()
{
   // Quadrature, basis function values for interior assembly
   QGauss<dim> quadrature_formula (FE_order+1);
   FEValues<dim> fe_values (fe, quadrature_formula,
                            update_values | update_gradients |
                            update_quadrature_points | update_JxW_values);

   const unsigned int dofs_per_cell = fe.dofs_per_cell,
                      n_quad = quadrature_formula.size();
   
   std::vector<types::global_dof_index> local_dof_indices (dofs_per_cell);
   
   FullMatrix<PetscScalar> cell_A (dofs_per_cell, dofs_per_cell),
                           cell_M (dofs_per_cell, dofs_per_cell);

   const FEValuesExtractors::Vector U (0);
   for (const auto &cell: dof_handler.active_cell_iterators())
   {
      if (cell -> is_locally_owned())
      {
         fe_values.reinit(cell);
         cell_A = 0.0;
         cell_M = 0.0;

         for (unsigned int q = 0; q < n_quad; ++q)
         {
            // Note: subscript i associates with test functions,
            //                 j associates with trial functions.
            for (unsigned int i = 0; i < dofs_per_cell; ++i)
            {
               for (unsigned int j = 0; j < dofs_per_cell; ++j)
               {
                  // Note: C[∇E] = parameters.elasticity_T * grad_U
                  cell_A(i, j) +=
                           parameters.elasticity_T *
                           fe_values[U].symmetric_gradient(j, q) *
                           fe_values[U].symmetric_gradient(i, q) *
                           fe_values.JxW(q);
                  cell_M(i, j) +=
                           parameters.rho_quartz *
                           fe_values[U].value(j, q) *
                           fe_values[U].value(i, q) *
                           fe_values.JxW(q);
               }
            }
         }

         cell -> get_dof_indices(local_dof_indices);
         hanging_nodes.distribute_local_to_global(cell_A, local_dof_indices, A);
         hanging_nodes.distribute_local_to_global(cell_M, local_dof_indices, M);
      }
   }

   A.compress(VectorOperation::add);
   M.compress(VectorOperation::add);

   // Move spiruous eigenvalues introduced by the Dirichlet BCs away from the
   // region of interest. See deal.II step 36 for a more detailed explanation.
   for (auto i : locally_owned_dofs)
      if (hanging_nodes.is_constrained(i))
      {
         A.set(i, i, 1.0e+14);
         M.set(i, i, 1.0);
      }

   A.compress(VectorOperation::insert);
   M.compress(VectorOperation::insert);
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void EigenFrequency<dim> :: solve()
{
   timer.enter_subsection("Solve");

   SolverControl solver_options (dof_handler.n_dofs(), 1e-9);
   SLEPcWrappers::SolverKrylovSchur eigensolver (solver_options, MPI_COMM_WORLD);
   eigensolver.set_which_eigenpairs(EPS_SMALLEST_REAL);
   eigensolver.set_problem_type(EPS_GHEP);

   // Number of eigenvectors to solve for.
   CHKERRXX(PetscOptionsGetInt(NULL, NULL, "-nEigenvalues", &n_eigs, NULL));

   std::vector<PetscScalar> eigenvalues (n_eigs);

   // Create a distributed eigenvector
   std::vector<PETScWrappers::MPI::Vector> local_eigenvectors (n_eigs);
   for (auto &eigenvector : local_eigenvectors)
      eigenvector.reinit(locally_owned_dofs, MPI_COMM_WORLD);

   // Compute the eigen-pairs
   eigensolver.solve(A, M, eigenvalues, local_eigenvectors, n_eigs);

   timer.leave_subsection();

   PetscScalar eigenvalue = eigenvalues[1];
   pcout << "Eigenvalue: " << real(sqrt(eigenvalue)) << endl;
   pcout << "Frequency : " << real(sqrt(eigenvalue)) / (2*numbers::PI) << endl;

   eigenvectors.resize(n_eigs);
   for (PetscInt i = 0; i < n_eigs; ++i)
   {
      Vector<PetscScalar> global_eigenvector(dof_handler.n_dofs());
      global_eigenvector = local_eigenvectors[i];
      hanging_nodes.distribute(global_eigenvector);

      eigenvectors[i].reinit(dof_handler.n_dofs());
      for (types::global_dof_index j = 0; j < dof_handler.n_dofs(); ++j)
         eigenvectors[i][j] = global_eigenvector[j].real();
   }
   
   // Store the eigenfrequency to be used by a subsequent stage.
   if (rank == 0)
   {
      std::ofstream output_w("../w_value.txt");
      output_w << std::setprecision(10) << real(sqrt(eigenvalue)) << endl;
      output_w.close();
   }
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void EigenFrequency<dim> :: refine_grid()
{
   Vector<float> estimated_error_per_cell (triangulation.n_active_cells());
   KellyErrorEstimator<dim> ::estimate(dof_handler,
                        QGauss<dim-1> (FE_order+1),
                        std::map<types::boundary_id, const Function<dim>*> (),
                        eigenvectors[1],
                        estimated_error_per_cell);

   // Percentage of cells to be refined.
   PetscReal ref_percent = 0;
   CHKERRXX(PetscOptionsGetReal(NULL, NULL, "-refine_percent",
            &ref_percent, NULL));

   GridRefinement::refine_and_coarsen_fixed_number(triangulation,
                                                   estimated_error_per_cell,
                                                   ref_percent, 0.0);
   triangulation.execute_coarsening_and_refinement();
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void EigenFrequency<dim> ::
         child_adaptivity(const typename DoFHandler<dim>::cell_iterator &cell,
                          std::ofstream &output)
{
   if (cell -> has_children())
   {
      // Export cell level & its center.
      output << cell -> level() << " " << cell -> center() << endl;

      // Cycle through its children.
      for (unsigned int i = 0; i < pow(2, dim); ++i)
         child_adaptivity(cell -> child(i), output);
   }
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void EigenFrequency<dim> :: export_solution()
{
   // Export adaptivity. Since we use shared::triangulation, only one processor
   // needs to output the data.
   PetscBool export_adaptivity = PETSC_TRUE;
   CHKERRXX(PetscOptionsGetBool(NULL, NULL, "-ExportAdaptivity",
                                &export_adaptivity, NULL));
   if (rank == 0 && export_adaptivity)
   {
      std::ofstream output("../refinement/refinements.dat");
      output.precision(10);

      // Only export indicators for cells that have children. We will need to
      // know how many of them there are.
      unsigned int n_coarse_refined_cells = 0;
      for (const auto &cell : dof_handler.cell_iterators())
      {
         if (cell -> level() == 0 && cell -> has_children())
         {
            ++n_coarse_refined_cells;
            output << cell -> center() << endl;
            
            // Check if this (level 1 of refinement) cell has any further
            // children.
            for (unsigned int i = 0; i < pow(2, dim); ++i)
               child_adaptivity(cell -> child(i), output);
            output << endl;
         }
      }
      output.close();

      // Output number of global refinement levels, and number of coarse cells
      // that have been refined.
      std::ofstream output2("../refinement/data.dat");
      output2 << triangulation.n_global_levels() << endl
              << n_coarse_refined_cells;
      output2.close();
   }

   // Export eigenvectors to pvtu format (optional).
   PetscBool export_sol = PETSC_TRUE;
   CHKERRXX(PetscOptionsGetBool(NULL, NULL, "-ExportEigenVectors",
                                &export_sol, NULL));

   if (export_sol)
   {
      std::vector<DataComponentInterpretation::DataComponentInterpretation>
         data_component_interpretation(dim,
                     DataComponentInterpretation::component_is_part_of_vector);

      DataOut<dim> data_out;
      data_out.attach_dof_handler(dof_handler);

      for (unsigned int i = 0; i < eigenvectors.size(); ++i)
         data_out.add_data_vector(eigenvectors[i],
                                  "u" + Utilities::int_to_string(i, 3),
                                  DataOut<dim>::type_dof_data,
                                  data_component_interpretation);
      data_out.build_patches();

      std::string filename = "../output/solution_proc_"
                      + Utilities::int_to_string(rank, 3)
                      + ".vtu";
      std::ofstream output(filename.c_str());
      data_out.write_vtu(output);

      if (rank == 0)
      {
         std::vector<std::string> filenames;
         for (unsigned int i = 0; i < world_size; ++i)
         {
            filenames.push_back("solution_proc_"
                              + Utilities::int_to_string(i, 3)
                              + ".vtu");
         }

         std::string main_filename = "../output/w_solution.pvtu";
         std::ofstream full_output(main_filename);
         data_out.write_pvtu_record(full_output, filenames);
      }
   }
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void EigenFrequency<dim> :: run()
{
   timer.enter_subsection("Import Grid");
   import_grid();
   timer.leave_subsection();

   PetscInt n_adapt_ref = 0;
   CHKERRXX(PetscOptionsGetInt(NULL, NULL, "-nAdaptiveRefinements",
            &n_adapt_ref, NULL));

   for (PetscInt refinement_step = 0; refinement_step <= n_adapt_ref;
        ++refinement_step)
   {
      if (refinement_step > 0)
      {
         timer.print_summary();
         timer.reset();
         
         pcout << "Refinement step " << refinement_step << ": " << endl;
         timer.enter_subsection("Refine mesh");
         refine_grid();
         timer.leave_subsection();
      }

      timer.enter_subsection("Setup System");
      setup_system();   
      timer.leave_subsection();
      
      timer.enter_subsection("Assemble System");
      assemble_system();
      pcout << "System Assembled" << endl << endl;
      timer.leave_subsection();

      solve();
      pcout << "Solved" << endl;
   }

   timer.enter_subsection("Export Solution");
   export_solution();
   timer.leave_subsection();
}
