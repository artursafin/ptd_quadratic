#include "EigenFrequency.h"

int main(int argc, char *argv[])
{
   try
   {
      Utilities::MPI::MPI_InitFinalize mpi_initialization (argc, argv, 1);
      {
         EigenFrequency<3> pde;
         pde.run();
      }
   }
   catch (std::exception &exc)
   {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Exception on processing: " << std::endl
                << exc.what() << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
   }
   catch (...)
   {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Unknown exception!" << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
   }
}
