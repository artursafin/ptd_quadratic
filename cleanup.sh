for folderName in EigenProblem PT_Eqns QTF_Displacement; do
	rm -rf $folderName/build/ $folderName/output/
done

rm -rf EigenProblem/refinement/
rm -f EigenProblem/w_value.txt
rm -rf PT_Eqns/TFsolution/

