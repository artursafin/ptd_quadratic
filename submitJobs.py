#!/usr/local/bin/python
from commands import getstatusoutput
from re import findall
from sys import argv

# submitting this script: run 'python submitJobs.py A B'
# where
# A is the vertical shift (from 0 to 20)
# B is job dependency (optional): the script will start execution only
# after job #B has finished.

xshift = 0;
zshift = argv[1];
amb_pressure = 5.0;

# This python script submits the following jobs to run in sequence:
# sbatch 1.txt xshift zshift
# sbatch 2.txt xshift zshift amb_pressure
# sbatch 3.txt xshift zshift amb_pressure

appendXZ = f'{xshift} {zshift}'
appendXZP = f'{append} {amb_pressure}'

# submit 1st module
if (len(argv) == 2):
	cmd = f'sbatch 1.txt {appendXZ}'
else:
	cmd = f'sbatch --dependency=afterok:{argv[2]} 1.txt {appendXZ}'

status, joboutput = getstatusoutput(cmd)
if (status != 0 ):
	print('Could not submit the generalized eigenproblem')

else:
	jobnumber = findall(r'\d+', joboutput)[-1];
	print(f'Submitted generalized eigenproblem,\tjob #{jobnumber}')

	# submit 2nd module
	cmd2 = f'sbatch --dependency=afterok:{jobnumber} 2.txt {appendXZP}'
	status2, joboutput2 = getstatusoutput(cmd2)
	
	if (status2 != 0 ):
		print('Could not submit the thermoacoustic problem')
	else:
		jobnumber2 = findall(r'\d+', joboutput2)[-1]
		print(f'Submitted thermoacoustic problem,\tjob #{jobnumber2}')
		
		# submit 3rd module
		cmd3 = f'sbatch --dependency=afterok:{jobnumber2} 3.txt {appendXZP}'
		status3, joboutput3 = getstatusoutput(cmd3)

		if (status3 != 0 ):
			print('Could not submit the elasticity module')
		else:
			jobnumber3 = findall(r'\d+', joboutput3)[-1]
			print(f'Submitted elasticity module,\t\tjob #{jobnumber3}')

