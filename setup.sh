# (Re-) Compile all 3 programs. If the '*/build' directories do not exist,
# create them.

for folderName in EigenProblem PT_Eqns QTF_Displacement; do
	if [ ! -d "$folderName/build" ]; then
		mkdir $folderName/build
	fi

   (
	   cd $folderName/build
	   rm cmake_output.txt
	   cmake .. >> cmake_output.txt 2>&1
	   more cmake_output.txt | egrep -i "error|generating"
	   make
   )
done

