from paraview.simple import *
import sys

reader = OpenDataFile("output/w_solution.pvtu")

w = 0.6e-3
t = 0.34e-3
g = 0.3e-3
hu = 3.75e-3
hp = 2.33e-3
h0 = 5e-4

xMax = 2 * (2*w + g)
zMax = 8e-3

xc = xMax/2 + g/2;
yc = 1e-5;
zc = h0 + hp + g/2;

slice_filter = Slice(reader)
slice_filter.SliceType.Normal = [0.0, 1.0, 0.0]
slice_filter.SliceType.Origin = [xc, yc, zc];
slice_filter.SliceOffsetValues = [0.0]
slice_filter.SliceType = "Plane"

slice_filter2 = Slice(slice_filter)
slice_filter2.SliceType.Normal = [0.0, 0.0, 1.0]
slice_filter2.SliceType.Origin = [xc, yc, zc];
slice_filter2.SliceOffsetValues = [0.0]
slice_filter2.SliceType = "Plane"

writer = CreateWriter("solution_slice_Coupled.csv", slice_filter2)
writer.UpdatePipeline()

import numpy
import csv
csvData = numpy.loadtxt("solution_slice_Coupled.csv", delimiter=",", skiprows=1, dtype=None)
csvData=csvData[csvData[:, 4].argsort()]
with open("solution_slice_Coupled.csv", 'wb') as f:
        f.write(b'"P1","P2","T1","T2","X","Y","Z"\n')
        numpy.savetxt(f, csvData, delimiter=",", fmt="%g")

