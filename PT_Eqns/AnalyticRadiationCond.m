function [dom, P_an, T_an] = AnalyticRadiationCond(R_dim, plot_stuff)

% Constants for the source
alpha_eff = .05;
W_L = .03;
rho = 0.007662;          % Density of gas
C_p = 1039.7;           % Specific heat capacity of gas

% Constants for the MI
c = 349.0;              % Speed of sound in gas
gamma = 1.4;            % Ratio of specific heats
alpha = 2.274;
lv = 1.383e-6;

K_air = 0.025367;
lh = K_air / (rho * C_p * c);

% Laser properties and offset
w0 =  7.5e-6;
yR = pi * w0 * w0 / 1.57e-6; % pi * sigma ^2 / lambda
y = 0;%(3/2) * 0.34e-3;         % distance from the center of the laser to
                             % the plane cut where MI eqns are solved
omega = 2.061e+5;

% Options
number_of_steps = 200;
abs_tol = 1e-15; rel_tol = 1e-13;

% Non-dimensionalization
sigma_dim = w0 * sqrt(1+(y/yR)^2); % Width of laser beam (dimensionalized)
R = R_dim * omega / c;
sigma = sigma_dim * omega / c;
Omega = omega * lh / c;
Lambda = omega * lv / c;

% Compute kt, kp, mt, mp
Q = sqrt((1-1i*gamma*Omega-1i*Lambda)^2+4*(1i*Omega+gamma*Omega*Lambda));
kt=sqrt((1i/(2*Omega))*(1-1i*Lambda-1i*gamma*Omega+Q)/(1-1i*gamma*Lambda));
kp=sqrt((1i/(2*Omega))*(1-1i*Lambda-1i*gamma*Omega-Q)/(1-1i*gamma*Lambda));

mp = gamma/(gamma-1)*(1+1i*Omega*kp^2);
mt = gamma/(gamma-1)*(1+1i*Omega*kt^2);

E =-1i*gamma*Lambda/(Omega*(1-1i*gamma*Lambda));
S = @(s) exp(-2*s.^2/(sigma^2));
A = (alpha_eff * W_L) / (pi * rho * C_p * sigma_dim^2) * (alpha / omega);

integrand1 = @(s) s.*besselh(0,1,kp*s).*S(s);
integrand2 = @(s) s.*besselj(0,kp*s).*S(s);
integrand3 = @(s) s.*besselh(0,1,kt*s).*S(s);
integrand4 = @(s) s.*besselj(0,kt*s).*S(s);

c1_f = @(r) pi/(2*1i*(mt-mp))*(E - mt/Omega) * ...
       integral(integrand1,0,r,'AbsTol', abs_tol, 'RelTol', rel_tol);
c2_f = @(r) -pi/(2*1i*(mt-mp))*(E - mt/Omega) * ...
       integral(integrand2,0,r,'AbsTol', abs_tol, 'RelTol', rel_tol);
c3_f = @(r) -pi/(2*1i*(mt-mp))*(E - mp/Omega) * ...
       integral(integrand3,0,r,'AbsTol', abs_tol, 'RelTol', rel_tol);
c4_f = @(r) pi/(2*1i*(mt-mp))*(E - mp/Omega) * ...
       integral(integrand4,0,r,'AbsTol', abs_tol, 'RelTol', rel_tol);

% Radius at which the c_k(r) ~ c_k
R_inf = 5e-1;
c1 = c1_f(R_inf);
c2 = c2_f(R_inf);
c3 = c3_f(R_inf);
c4 = c4_f(R_inf);

% Declare non-dimensionalized versions of the solutions
P = @(r) (c1_f(r)-c1)*mp * besselj(0, kp*r) + ...
          c2_f(r)*mp * besselh(0,1,kp*r) + ...
         (c3_f(r)-c3)*mt * besselj(0, kt*r) + ...
          c4_f(r)*mt * besselh(0,1,kt*r);
P_inf = @(r) c2*mp * besselh(0,1,kp*r) + c4*mt * besselh(0,1,kt*r);
T = @(r) (c1_f(r)-c1) * besselj(0, kp*r) + ...
          c2_f(r) * besselh(0,1,kp*r) + ...
         (c3_f(r)-c3) * besselj(0, kt*r) + ...
          c4_f(r) * besselh(0,1,kt*r);
T_inf = @(r) c2 * besselh(0,1,kp*r) + c4 * besselh(0,1,kt*r);

% Finally we can compute the solution
solution = zeros(number_of_steps+1, 4);
P_an = zeros(number_of_steps+1, 1);
T_an = zeros(number_of_steps+1, 1);

for n = 0:number_of_steps
    if (n == 0)
        tempT = (A/alpha)*(-c1-c3);
        tempP = -A*(c1*mp+c3*mt);
    else
        dist = (n/number_of_steps) * R;
        if (dist < R_inf)
            tempT = (A/alpha)*T(dist);
            tempP = A*P(dist);
        else
            tempT = (A/alpha)*T_inf(dist);
            tempP = A*P_inf(dist);
        end
    end
    solution(n+1,1) = abs(tempP);
    solution(n+1,2) = atan2(imag(tempP), real(tempP));
    solution(n+1,3) = abs(tempT);
    solution(n+1,4) = atan2(imag(tempT), real(tempT));
    
    P_an(n+1) = tempP;
    T_an(n+1) = tempT;
end
dom = (0:(R/number_of_steps):R)'*(c/omega);
