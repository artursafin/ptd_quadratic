function h = Compare(component, re_paraview, plotAnalytic)
% component - 1 = abs(P)
%             2 = abs(T)
%             3 = phase(P)
%             4 = phase(T)
% pltoAnalytic - plot analytic free-space solution
% re_paraview - re-generate slices by running pythonSlice.py

if (re_paraview)
   !python pythonSlice.py
end

% Import P-T solution solution
A=importdata('solution_slice_Coupled.csv', ',', 1);
Pmag = A.data(:,1);
Tmag = A.data(:,2);
Pphase = A.data(:,3);
Tphase = A.data(:,4);
X = A.data(:,5) * 1000;

% We only want to plot the solution for x>x_c. For the current mesh, it is
% at 1.5mm. So we shift and truncate.
X = X - 1.5;
index = find(X<0, 1, 'last');
X = X(index+1:end);
Pmag = Pmag(index+1:end);
Tmag = Tmag(index+1:end);
Pphase = Pphase(index+1:end);
Tphase = Tphase(index+1:end);

% Dimensions of the QTF
g = 0.3e-3 * 1000;
w = 0.6e-3 * 1000;

% We also assume that the laser is centered for x>x_c.
xMax = (w+g/2) * 2;
x_right = xMax/2; % rightmost point of the QTF

% Analytic
if (plotAnalytic)
	[dom, P_an, T_an] = AnalyticRadiationCond(X(end)/1000, 0);
	dom = dom * 1000;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
h = figure(1);
if (component == 1)
	% Compute amplitudes and kill values inside the QTF
	Pmag(X>=(g/2+eps) & X<=(x_right-eps)) = inf;
	plot(X, Pmag, 'b-*', 'Linewidth', 2, 'MarkerSize', 3);

	hold on;
	if (plotAnalytic)
		plot(dom, abs(P_an), 'k--', 'LineWidth', 1);
	end
	
	xlim([0 X(end)]);
	ylims = get(gca, 'ylim');
	xlims = get(gca, 'xlim');

	% Label the PML region
	p=patch([xMax xlims(2) xlims(2) xMax], [ylims(1) ylims ylims(2)], ...
		[0.54 0.27	0.07], 'EdgeColor','none');
	alpha(p, 0.1)

	text((xlims(2)+xMax)/2, mean(ylims), {'PML','Region'}, ...
		'HorizontalAlignment', 'center');
	title('Amplitude of pressure', 'FontWeight', 'normal');
	text(g/2+w/2, ylims(2)*.75, 'QTF', 'HorizontalAlignment', 'center');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif (component == 2)
	semilogy(X, Tmag+eps, 'b-*', 'Linewidth', 2, 'MarkerSize', 3);
	hold on;
	
	if (plotAnalytic)
		semilogy(dom, abs(T_an), 'k--', 'LineWidth', 1);
	end
	
	xlim([0 X(end)]);
	ylim([1e-11 max(Tmag)*3])
	
	ylims = get(gca, 'ylim');
	xlims = get(gca, 'xlim');

	% Label the PML region
	p=patch([xMax xlims(2) xlims(2) xMax], [ylims(1) ylims ylims(2)], ...
		[0.54 0.27	0.07], 'EdgeColor','none');
	alpha(p, 0.1)
	text((xlims(2)+xMax)/2, (ylims(1)^7*ylims(2))^(1/8), {'PML','Region'}, ...
		'HorizontalAlignment', 'center');

	text(g/2+w/2, (ylims(1)*ylims(2))^(0.5), 'QTF', 'HorizontalAlignment', 'center');
	ylabel('Amplitude of temperature [log scale]', 'FontWeight', 'normal');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif (component == 3)
	Pphase(X>=(g/2+eps) & X<=(x_right-eps)) = inf;
	plot(X, Pphase, 'bx--', 'Linewidth', 2, 'Markersize', 3);
	hold on;

	if (plotAnalytic)
		plot(dom, angle(P_an)-pi, 'k-', 'LineWidth', 2);
	end
	xlim([0, xMax]);
	title('Pressure, Phase', 'FontWeight', 'normal');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif (component == 4)
	plot(X, Tphase, 'bx--', 'Linewidth', 2, 'Markersize', 3);
	hold on;
	
	if (plotAnalytic)
		plot(dom, angle(T_an)-pi, 'k-', 'LineWidth', 2);
	end

	xlim([0, xMax]);
	title('Temperature, Phase', 'FontWeight', 'normal');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fsize = 11;

ylims = get(gca, 'ylim');
p=patch([g/2 x_right x_right g/2], ...
		[ylims(1) ylims ylims(2)], [0.0, 0.0, 1], ...
		'EdgeColor','none');
alpha(p, 0.4)


fig = gcf;
sizeFile = [5.45 3];
set(fig, 'Units', 'inches', 'Position',[0 0 sizeFile], 'PaperPositionMode', 'auto')

set(findall(fig, '-property', 'FontSize'), 'FontSize', fsize)
set(findall(fig, '-property', 'FontName'), 'FontName', 'Helvetica')
fig.PaperUnits = 'inches';
fig.PaperPosition = [0 0 sizeFile];
fig.PaperSize = sizeFile;
ylims = get(gca, 'ylim');
plot([0 0], ylims, 'Color', [0.8 0.8 0.8], 'LineWidth', 0.25);
