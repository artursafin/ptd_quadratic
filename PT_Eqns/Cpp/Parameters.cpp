/* Parameters class. We could use the official ParameterHandler class, but it is
   easier to program the parameters ourselves, since we use PETSc functionality
   for accessing run-time parameters.
*/
using namespace dealii;

class Parameters
{
public:
   void setup_PT_parameters();      // Initialize constants for the PT-equations
   void setup();                    // Initialize more complex data-types
   void import_Q();                 // Import the Q-factor
   double read_omega();             // Read in the numeric ω-value

   // Ambient temperature, pressure.
   const double T_0 = 293.15;       // Ambient temperature in Kelvin
   double P_0;                      // Ambient pressure in pascals

   // Constants for the coupled P-T equations: c, μ, η.
   const double gamma = 1.4;        // Gas expansion factor
   const double mu = 1.785e-5;      // Shear viscosity at 20 °C for nitrogen
   const double eta = 1.317e-5;     // Bulk viscosity at 20 °C for nitrogen

   // Air constants: C_p, κ.
   const double C_p_air = 1039.7;   // Specific heat capacity of air
   const double K_air = 0.025367;   // Thermal conductivity of air
   const double rho_at_stp = 1.165; // Density of nitrogen gas at stp

   double lv, alpha, rho_air, c, lh;

   // Properties of quartz: ρ_quartz, C_p_quartz, κ_quartz.
   const double rho_quartz = 2650.0;// Density of quartz
   const double C_p_quartz = 733.0; // Specific heat capacity of quartz
   const double nu = 7e-6;          // Effective piezoelectric constant
   double Q;                        // Tuning fork quality factor
   dealii::Tensor<2, 3> K_tf;

   // Properties of the laser.
   const double w_0 = 7.5e-6;       // Width of the beam (aka beam waist radius)
   const double lambda = 1.57e-6;   // Laser's wavelength of the radiation
                                    // Note: this is 1/f, where f from Kosterev
                                    // & Doty 2010.
   double alpha_eff, W_L;
   
   // Dimensions of the geometry of the QTF.
   const double w = 0.6e-3;         // Width of the tine, along x-axis
   const double t = 0.34e-3;        // Thickness of the tuning fork along y-axis
   const double g = 0.3e-3;         // Gap between the tines
   const double hu = 3.75e-3;       // Height of tine from top, down until it
                                    // begins to bend
   const double hp = 2.33e-3;       // Height of the TF from its lowest point to
                                    // the gap between the tines

   // This number will say how high the laser is in relation to the gap between
   // the tines. A height of zero corresponds to the center of the half-cylinder,
   // and a height of 20 is the top of the QTF.
   PetscInt height_z_index;

   // The center of the laser. Note: this operation is done entirely in
   // this class, and does not rely on any external parameters.
   Point<3> laser_center;

   // Elasticity tensor.
   SymmetricTensor<4, 3> elasticity_T;
   
   // Thermal expansion tensor
   SymmetricTensor<2, 3> alpha_T;
};

void Parameters :: setup_PT_parameters()
{
   // Import parameters: P₀, α_eff, W_L, Q. We also check if these values have
   // ctually been set; otherwise the computation is useless.
   PetscBool found_value;
   
   // Ambient pressure should be given in Torr.
   PetscOptionsGetReal(NULL, NULL, "-ambient_P", &P_0, &found_value);
   if (found_value == PETSC_FALSE)
		throw std::invalid_argument("Ambient pressure value not supplied.");
   P_0 *= 133.322;   // Convert to pascals.

   PetscOptionsGetReal(NULL, NULL, "-alpha_eff", &alpha_eff, &found_value);
   if (found_value == PETSC_FALSE)
		throw std::invalid_argument("alpha_eff value not supplied.");

   PetscOptionsGetReal(NULL, NULL, "-laser_power", &W_L, &found_value);
   if (found_value == PETSC_FALSE)
		throw std::invalid_argument("Laser_power value not supplied.");

   // Compute lᵥ, γ, α, ρ.
   rho_air = rho_at_stp * (P_0 / 101325.0);
   c = std::sqrt(gamma * P_0 / rho_air);
   alpha = P_0 / T_0;
   lv = (eta + (4./3.)*mu) / (rho_air * c);
   lh = K_air / (rho_air * C_p_air * c);
}

void Parameters :: import_Q()
{
   PetscBool found_value;
   PetscOptionsGetReal(NULL, NULL, "-Qvalue", &Q, &found_value);
   if (found_value == PETSC_FALSE)
		throw std::invalid_argument("Ambient pressure value not supplied.");
}

void Parameters :: setup()
{
   // Offset from the ROTADE center
   PetscInt offset_x_mult = 0;
   height_z_index = 0;
   CHKERRXX(PetscOptionsGetInt(NULL, NULL, "-offsetx",
            &offset_x_mult, NULL));
   CHKERRXX(PetscOptionsGetInt(NULL, NULL, "-offsetz",
            &height_z_index, NULL));

   const double offset_x = (g/5) * (double) offset_x_mult;
   const double offset_z = (hu/20) * (double) height_z_index;

   // Compute the center of the laser
   laser_center(0) = 2*w + g + offset_x;
   laser_center(1) = 0;
   laser_center(2) = hp + g/2 + offset_z;
   
   // Set up thermal conductivity of quartz
   K_tf.clear();
   K_tf[0][0] = 6.5;
   K_tf[1][1] = 6.5;
   K_tf[2][2] = 11.3;

   // Set up thermal expansion tensor.
   alpha_T.clear();
   alpha_T[0][0] = 13.7e-6;
   alpha_T[1][1] = 13.7e-6;
   alpha_T[2][2] = 7.4e-6;

   // Elasticity tensor. Per Gautschi
   const double C11 = 86.80 * 1e+9,
                C12 = 7.04 * 1e+9,
                C13 = 11.91 * 1e+9,
                C14 = -18.04 * 1e+9,
                C33 = 105.75 * 1e+9,
                C44 = 58.20 * 1e+9,
                C66 = 39.88 * 1e+9;

   elasticity_T.clear();
   elasticity_T[0][0][0][0] = C11;
   elasticity_T[0][0][1][1] = C12;
   elasticity_T[0][0][2][2] = C13;
   elasticity_T[0][0][1][2] = C14;

   elasticity_T[1][1][0][0] = C12;
   elasticity_T[1][1][1][1] = C11;
   elasticity_T[1][1][2][2] = C13;
   elasticity_T[1][1][1][2] = -C14;

   elasticity_T[2][2][0][0] = C13;
   elasticity_T[2][2][1][1] = C13;
   elasticity_T[2][2][2][2] = C33;

   elasticity_T[1][2][0][0] = C14;
   elasticity_T[1][2][1][1] = -C14;
   elasticity_T[1][2][1][2] = C44;

   elasticity_T[0][2][0][2] = C44;
   elasticity_T[0][2][0][1] = C14;

   elasticity_T[0][1][0][1] = C66;
   elasticity_T[0][1][0][2] = C14;
}

double Parameters :: read_omega()
{
   // Import ω from the eigen-frequency stage.
	std::ifstream read_omega("../../EigenProblem/w_value.txt");

	if(!read_omega.is_open())
		throw std::invalid_argument("Could not find ω, idiot.");
	double omega;
	read_omega >> omega;
	read_omega.close();

   return omega;
}
