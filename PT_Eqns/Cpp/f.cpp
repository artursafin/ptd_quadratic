/* RHS of system; in form [ ia₃S]
                          [ -b₃S].
   S is the source function: S = (A/w²(y))exp[-2((x-x₀)² + (z-z₀)²) / w²(y)],
   where w²(y) = (w₀)²(1 + [(y-y₀)/y_R]²).
   
   Note: Number = complex<double>
*/

using namespace dealii;

template <int dim, typename Number>
class f : public Function<dim, Number>
{
public:
	f(Parameters &parameters, const double a3_arg);
	virtual void vector_value(const Point<dim> &p,
							        Vector<Number> &values) const;
	virtual void vector_value_list(const std::vector<Point<dim> > &points,
                             std::vector<Vector<Number> > &value_list) const;

private:
	double a3, A, yR, w0;
	
   // Since the thickness of the laser varies, the source term depends on its
   // distance along the laser axis.
	double x_laser, y_laser, z_laser;
};

template <int dim, typename Number>
f <dim, Number> :: f(Parameters& parameters, const double a3_arg)
   : Function<dim, Number> (2)
{
	a3 = a3_arg;

	A = (parameters.alpha_eff * parameters.W_L) / (numbers::PI);
	w0 = parameters.w_0;
   yR = numbers::PI * w0 * w0 / parameters.lambda;
   
   x_laser = parameters.laser_center(0);
   y_laser = parameters.laser_center(1);
   z_laser = parameters.laser_center(2);
}

template <int dim, typename Number>
void f<dim, Number> :: vector_value(const Point <dim> &p,
					      		         Vector <Number> &values) const
{
   const double w_sqd = w0 * w0 * (1 + pow((p(1)-y_laser) / yR, 2));
	const double S = A / w_sqd /
	                 exp(2*(pow(p(0)-x_laser,2) + pow(p(2)-z_laser,2))/w_sqd);

	values(0) = Number (0, a3 * S);
	values(1) = Number (-S, 0);
}

template <int dim, typename Number>
void f<dim, Number> :: vector_value_list(const std::vector<Point<dim> > &points,
					            std::vector<Vector<Number> > &value_list) const
{
	for (unsigned int k = 0; k < points.size(); k++)
		f<dim, Number> :: vector_value(points[k], value_list[k]);
}
