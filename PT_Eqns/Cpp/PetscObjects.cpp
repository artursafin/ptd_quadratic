
#include <petsc.h>

#define COLOR_GREEN  "\x1B[32m"
#define COLOR_CYAN   "\x1b[36m"
#define COLOR_RED    "\x1b[31m"
#define COLOR_RESET  "\x1b[0m"

Vec temp, temp_r,
    rhs, rhs_r,
    y_r;

typedef struct
{
   IS fs_comp_IS, fs_pml_IS,
      pr_comp_IS, pr_pml_IS;
   
   Mat Pcomp, Pcomp_r,
       MassM, MassM_r, MassComp, MassComp_r,
       Ppml_r, A21_r;

   KSP ksp_pml, ksp_comp, ksp_mass;
   PC pc_pml, pc_comp, pc_mass;
} PetscData;

// ██████████████████████████████████████████████████████████████████████████ //
// Monitor for the inversion of the RHS vector by the mass matrix
PetscErrorCode MonitorPressureComp(KSP, PetscInt n, PetscReal rnorm, void *)
{
   return PetscPrintf(MPI_COMM_WORLD, COLOR_CYAN
   "\t PressureComp GMRES+AMG: <%D> Relative error: %10.5e\n"
   COLOR_RESET, n, rnorm);
}

// ██████████████████████████████████████████████████████████████████████████ //
// Monitor for the inversion of the RHS vector by the mass matrix
PetscErrorCode MonitorPressureBlock(KSP, PetscInt n, PetscReal rnorm, void *)
{
   return PetscPrintf(MPI_COMM_WORLD, COLOR_RED
   "PressureBlock Custom: <%D> Relative error: %10.5e\n"
   COLOR_RESET, n, rnorm);
}

// ██████████████████████████████████████████████████████████████████████████ //
// Monitor for the inversion of the RHS vector by the mass matrix
PetscErrorCode MonitorTFBlock(KSP, PetscInt n, PetscReal rnorm, void *)
{
   return PetscPrintf(MPI_COMM_WORLD, COLOR_GREEN
   "TF Heat Eqn GMRES+AMG: <%D> Relative error: %10.5e\n"
   COLOR_RESET, n, rnorm);
}

// ██████████████████████████████████████████████████████████████████████████ //
// Monitor for the inversion of the RHS vector by the mass matrix
PetscErrorCode MonitorTempBlock(KSP, PetscInt n, PetscReal rnorm, void *)
{
   return PetscPrintf(MPI_COMM_WORLD, COLOR_CYAN
   "TempBlock GMRES+AMG: <%D> Relative error: %10.5e\n" COLOR_RESET, n, rnorm);
}

// ██████████████████████████████████████████████████████████████████████████ //
// Monitor for the inversion of the RHS vector by the mass matrix
PetscErrorCode MonitorMassInvert(KSP, PetscInt n, PetscReal rnorm, void *)
{
   return PetscPrintf(MPI_COMM_WORLD, COLOR_CYAN
      "MASS M SOLVER: <%D> Relative error: %10.5e\n" COLOR_RESET, n, rnorm);
}

// ██████████████████████████████████████████████████████████████████████████ //
void SetUpPressureBlock(PetscData *data)
{
   // Computational domain solver
   PetscErrorCode ierr = KSPCreate(MPI_COMM_WORLD, &data->ksp_comp);
   CHKERRXX(ierr);
   ierr = KSPSetOperators(data->ksp_comp, data->Pcomp_r, data->Pcomp_r);
   CHKERRXX(ierr);
   ierr = KSPSetType(data->ksp_comp, "gmres"); CHKERRXX(ierr);
   ierr = KSPSetTolerances(data->ksp_comp, 1e-4, 1e-15, PETSC_DEFAULT, 20);
   ierr = KSPGetPC(data->ksp_comp, &data->pc_comp); CHKERRXX(ierr);
   ierr = PCSetType(data->pc_comp, "gamg"); CHKERRXX(ierr);
   ierr = PCMGSetType(data->pc_comp, PC_MG_FULL); CHKERRXX(ierr);
   ierr = KSPSetFromOptions(data->ksp_comp); CHKERRXX(ierr);
   ierr = KSPMonitorCancel(data->ksp_comp); CHKERRXX(ierr);

   // PML domain solver
   ierr = KSPCreate(MPI_COMM_WORLD, &data->ksp_pml); CHKERRXX(ierr);
   ierr = KSPSetOperators(data->ksp_pml, data->Ppml_r, data->Ppml_r);
   CHKERRXX(ierr);
   ierr = KSPSetType(data->ksp_pml, "preonly"); CHKERRXX(ierr);
   ierr = KSPGetPC(data->ksp_pml, &data->pc_pml); CHKERRXX(ierr);
   ierr = PCSetType(data->pc_pml, "lu"); CHKERRXX(ierr);
   ierr = PCFactorSetMatSolverType(data->pc_pml, "mumps"); CHKERRXX(ierr);
   
   // Inversion by mass matrix
   ierr = KSPCreate(MPI_COMM_WORLD, &data->ksp_mass); CHKERRXX(ierr);
   ierr = KSPSetOperators(data->ksp_mass, data->MassM_r, data->MassM_r);
   CHKERRXX(ierr);
   ierr = KSPSetType(data->ksp_mass, "cg"); CHKERRXX(ierr);
   ierr = KSPGetPC(data->ksp_mass, &data->pc_mass); CHKERRXX(ierr);
   ierr = PCSetType(data->pc_mass, "jacobi"); CHKERRXX(ierr);
   ierr = KSPSetTolerances(data->ksp_mass, 1e-6, 1e-15, PETSC_DEFAULT, 20);
   ierr = KSPMonitorCancel(data->ksp_mass); CHKERRXX(ierr);
//   ierr = KSPMonitorSet(data->ksp_mass, MonitorMassInvert, NULL, NULL);
}

// ██████████████████████████████████████████████████████████████████████████ //
PetscErrorCode PressureBlock(PC pcs_hell, Vec x, Vec y)
{
   PetscData *data;
   CHKERRXX(PCShellGetContext(pcs_hell, (void**) &data));
   PetscErrorCode ierr;
   
   // Partition x into computational domain and PML region components.
   ierr = KSPSolve(data->ksp_mass, x, temp); CHKERRXX(ierr);

   ierr = VecGetSubVector(temp, data->fs_comp_IS, &temp_r); CHKERRXX(ierr);
   ierr = VecGetSubVector(rhs, data->fs_comp_IS, &rhs_r); CHKERRXX(ierr);
   
   // Compute x restricted to the computational domain
   ierr = MatMult(data->MassComp_r, temp_r, rhs_r); CHKERRXX(ierr);

   // Obtain approximate solution on the computational domain.
   ierr = VecGetSubVector(y, data->fs_comp_IS, &y_r); CHKERRXX(ierr);
   ierr = KSPSolve(data->ksp_comp, rhs_r, y_r); CHKERRXX(ierr);

   // Adjust RHS by subtracting A_21 * (solution in the comp domain)
   ierr = VecGetSubVector(temp, data->fs_pml_IS, &temp_r); CHKERRXX(ierr);
   ierr = MatMult(data->A21_r, y_r, temp_r); CHKERRXX(ierr);
   ierr = VecCopy(x, rhs); CHKERRXX(ierr);
   ierr = VecGetSubVector(rhs, data->fs_pml_IS, &rhs_r); CHKERRXX(ierr);
   ierr = VecAXPY(rhs_r, -1.0, temp_r); CHKERRXX(ierr);

   // Store the computational solution
   ierr = VecRestoreSubVector(y, data->fs_comp_IS, &y_r); CHKERRXX(ierr);

   // Solve the problem in the PML region
   ierr = VecGetSubVector(y, data->fs_pml_IS, &y_r); CHKERRXX(ierr);
   ierr = KSPSolve(data->ksp_pml, rhs_r, y_r); CHKERRXX(ierr);

   // Restore the pml solution
   return VecRestoreSubVector(y, data->fs_pml_IS, &y_r);
}

// ██████████████████████████████████████████████████████████████████████████ //
void petsc_destroy(PetscData *data)
{
   PetscErrorCode ierr;

   ierr = VecDestroy(&temp_r); CHKERRXX(ierr);
   ierr = VecDestroy(&temp); CHKERRXX(ierr);
   ierr = VecDestroy(&rhs); CHKERRXX(ierr);
   ierr = VecDestroy(&rhs_r); CHKERRXX(ierr);
   ierr = VecDestroy(&y_r); CHKERRXX(ierr);

   ierr = MatDestroy(&data->MassComp_r); CHKERRXX(ierr);
   ierr = MatDestroy(&data->MassM_r); CHKERRXX(ierr);

   ierr = MatDestroy(&data->Pcomp_r); CHKERRXX(ierr);
   ierr = MatDestroy(&data->Ppml_r); CHKERRXX(ierr);
   ierr = MatDestroy(&data->A21_r); CHKERRXX(ierr);

   ierr = KSPDestroy(&data->ksp_pml); CHKERRXX(ierr);
   ierr = KSPDestroy(&data->ksp_comp); CHKERRXX(ierr);
   ierr = KSPDestroy(&data->ksp_mass); CHKERRXX(ierr);

   ierr = ISDestroy(&data->fs_comp_IS); CHKERRXX(ierr);
   ierr = ISDestroy(&data->fs_pml_IS); CHKERRXX(ierr);
   ierr = ISDestroy(&data->pr_comp_IS); CHKERRXX(ierr);
   ierr = ISDestroy(&data->pr_pml_IS); CHKERRXX(ierr);
}
