/* Solve the Morse-Ingard equations in 3D with TF.
_________________________________________
| ∆+a₁-iΛγ∆ |     a₂  | | P | = |  ia₃S |
|    -b₁    |  K∆+ib₂ | | T | = | -b₃S  |
‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
Boundary conditions:
1) PML in x-z directions
2) Neumann BCs on the front and back faces
3) ∂ₙP=0 on the interface with the TF
4) Continuity of heat & flux on the interface with TF for T

In the TF, heat equation is
∇·(K_Q∇T) + ic₁T = 0

'Simple' Helmholtz preconditioner for pressure block.
Block Gauss Seidel for global problem.

*/

#include "ThermoAcoustic.h"

int main(int argc, char *argv[])
{
   try
   {
      Utilities::MPI::MPI_InitFinalize mpi_initialization (argc, argv, 1);
      PetscInitialize(&argc, &argv, (char*)0, "void");
      {
         ThermoAcoustic<3> pde;
         pde.run();
      }
      PetscFinalize();
   }
   catch (std::exception &exc)
   {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Exception on processing: " << std::endl
                << exc.what() << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
   }
   catch (...)
   {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Unknown exception!" << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
   }
}
