// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
ThermoAcoustic<dim> :: ~ThermoAcoustic()
{
   dof_handler.clear();
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void ThermoAcoustic<dim> ::
   initialize_sparsity(const AffineConstraints<PetscScalar> &hanging_nodes_arg,
                       DynamicSparsityPattern &dsp_arg) const
{
   dsp_arg.reinit(dof_handler.n_dofs(), dof_handler.n_dofs(), locally_relevant_dofs);
   DoFTools::make_sparsity_pattern(dof_handler, dsp_arg, hanging_nodes_arg, false);
   SparsityTools::distribute_sparsity_pattern(dsp_arg,
                                              locally_owned_dofs,
                                              MPI_COMM_WORLD,
                                              locally_relevant_dofs);
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void ThermoAcoustic<dim> :: IS_Convert(const std::vector<PetscInt> &vector_IS,
                                       IS &petsc_IS) const
{
   // Compatibility function - converts an index set in std::vector format to
   // a PETSc IS object.
   PetscInt *dof_indices = new PetscInt[vector_IS.size()];
   for (unsigned int i = 0; i < vector_IS.size(); ++i)
      dof_indices[i] = vector_IS[i];
   
   ISCreateGeneral(MPI_COMM_WORLD, vector_IS.size(),
                   dof_indices, PETSC_COPY_VALUES,
                   &petsc_IS);
}

// ██████████████████████████████████████████████████████████████████████████ //

struct KDtreeComparison
{
   // Comparison logic: point A is < than point B if A_z < B_z. If equal, then
   // A < B if A_x < B_x. If equal, then A < B if A_y < B_y. Since no point is
   // repeated, equality cannot be achieved in the last comparison.
   // This is identical to KD-tree sorting.
   bool operator()(const Point<3> &a, const Point<3> &b) const
   {
      if (a[2] < b[2])
         return true;
      else if (a[2] > b[2])
         return false;
      else
      {
         if (a[0] < b[0])
            return true;
         else if (a[0] > b[0])
            return false;
         else
         {
            return (a[1] < b[1]);
         }
      }
   }
};

