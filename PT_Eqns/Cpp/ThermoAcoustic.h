#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/utilities.h>
#include <deal.II/base/index_set.h>
#include <deal.II/base/timer.h>

#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/affine_constraints.h>
#include <deal.II/lac/sparsity_tools.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>

#include <deal.II/grid/grid_in.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_q.h>

#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/error_estimator.h>

// Export coarser solution
#include <deal.II/distributed/solution_transfer.h>

#include <deal.II/distributed/tria.h>
#include <deal.II/distributed/grid_refinement.h>

#include <deal.II/lac/petsc_sparse_matrix.h>
#include <deal.II/lac/petsc_vector.h>

#include <cmath>
#include <fstream>

#include "Parameters.cpp"
#include "f.cpp"
#include "f_constant.cpp"
#include "Export_TF.cpp"
#include "PetscObjects.cpp"
#include "PML_coeff.cpp"

using namespace dealii;
using std::endl;

// ██████████████████████████████████████████████████████████████████████████ //

template <int dim>
class ThermoAcoustic
{
public:
   ThermoAcoustic();
   ~ThermoAcoustic();
   void run();

private:
   void import_grid();
   void setup_system();
   
   // Determine sparsity pattern for a matrix or a preconditioner
   void initialize_sparsity(const AffineConstraints<PetscScalar> &hanging_nodes_arg,
                            DynamicSparsityPattern &dsp_arg) const;

   // Determine the index sets for the subdomains. Also impose BCs
   void index_sets();

   void assemble_system();
   void solve();

   void import_refinements();
   void refine_child(const unsigned int level,
                     const typename DoFHandler<dim>::cell_iterator &cell,
                     const Point<dim> &current_cell);
   void refine_grid();
   void export_solution();

   // Initializes index sets for the subdomains. Note that the parameter
   // vector_IS should already contain the index set for the *complex* vector
   void IS_Convert(const std::vector<PetscInt> &vector_IS, IS &petsc_IS) const;

   parallel::distributed::Triangulation<dim> triangulation;
   FESystem<dim> fe;                // FE Space (PxT) 
   
   DoFHandler<dim> dof_handler;
   
   // Variables for the global problem
   AffineConstraints<PetscScalar> hanging_nodes;  // For handling adaptive refinements
   DynamicSparsityPattern dsp;
   PETScWrappers::MPI::SparseMatrix A, MassM;
   PETScWrappers::MPI::Vector u, b;

   // Matrices, sparsity patterns and constraint operators for each subdomain.
   // Computational domain:
   AffineConstraints<PetscScalar> hanging_nodes_comp;
   PETScWrappers::MPI::SparseMatrix P_comp, Mass_comp;
   DynamicSparsityPattern dsp_comp;

   // Index sets that contain DoFs owned by a processor, and locally active DoFs
   IndexSet locally_relevant_dofs, locally_owned_dofs;

   // Current processor rank, number of processors
   const unsigned int rank, world_size;
   
   ConditionalOStream pcout;        // Displays output *only* from rank 0
   TimerOutput timer;               // Wall clock time
   
   Parameters parameters;           // Parameters class

   // Constant coefficients from the Morse-Ingard equation
   double a1, a2, a3;               // Pressure eq
   PetscScalar xi;
   
   double b1, b2, K_a;          // Heat eq

   // Constant coefficients for the TF domain
   double c1;

   // PML domain values
   double pml_a, pml_b;
   const double pml_thick = 6e-4, coeff = 50;

   // Approximate cell size
   const double l_mesh = 1.5e-4;

   // Index sets for the complex unknowns in each subdomain. 
   std::vector<PetscInt> fs_compIndices, fs_pmlIndices,
                         pr_comp_Indices, pr_pml_Indices,
                         pressure_Indices, temp_Indices,
                         TF_Indices;
   
   // Mesh subdomain and boundary markers
   const unsigned int tf_id = 1, comp_id = 2, pml_id = 3;
   const unsigned int pml_face_id = 3,
                      laser_face_id = 4,     // face for y=0
                      neumann_face_id = 5;   // face for y=y_s
};

#include "AuxiliaryFunctions.cpp"
#include "ThermoAcoustic.cpp"
