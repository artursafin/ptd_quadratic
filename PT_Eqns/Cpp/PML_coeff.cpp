// Attenuation function, which is of the form
//    β(x) = 1 + i*A(x-a)²/(b-a)².
// Here we just compute the imaginary part.

#include <deal.II/base/point.h>
#include <cmath>

using namespace dealii;

template <int dim>
class PML_coeff
{
public:
   PML_coeff(const double pml_a_t,
             const double pml_b_t,
             const double pml_thick_t,
             const double coeff_t);
   double value_x(const Point<dim> &p);
   double value_z(const Point<dim> &p);
   
private:
   const double pml_a, pml_b, pml_thick, coeff;
   const int power = 2;
};

template <int dim>
PML_coeff<dim> :: PML_coeff(const double pml_a_t,
                            const double pml_b_t,
                            const double pml_thick_t,
                            const double coeff_t)
 : pml_a(pml_a_t), pml_b(pml_b_t), pml_thick(pml_thick_t), coeff(coeff_t) {}

template <int dim>
double PML_coeff<dim> :: value_x(const Point<dim> &p)
{
   if ((p(0) > 0) && (p(0) < pml_a))
      return 0;
   else if (p(0) < 0)
      return coeff * pow(-p(0), power) / pow(pml_thick, power);
   else
      return coeff * pow(abs(p(0)) - pml_a, power)
                   / pow(pml_thick, power);
}

template <int dim>
double PML_coeff<dim> :: value_z(const Point<dim> &p)
{
   if ((p(2) > 0) && (p(2) < pml_b))
      return 0;
   else if (p(2) < 0)
      return coeff * pow(-p(2), power) / pow(pml_thick, power);
   else
      return coeff * pow(abs(p(2)) - pml_b, power)
                   / pow(pml_thick, power);
}
