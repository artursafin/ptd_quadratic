// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
ThermoAcoustic<dim> :: ThermoAcoustic() :
   triangulation(MPI_COMM_WORLD),
   fe(FE_Q<dim> (1), 1,    // Pressure block
      FE_Q<dim> (2), 1),   // Temperature block
   dof_handler(triangulation),
   rank(Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)),
   world_size(Utilities::MPI::n_mpi_processes(MPI_COMM_WORLD)),
   pcout(std::cout, rank == 0),
   timer(MPI_COMM_WORLD, pcout, TimerOutput::summary, TimerOutput::wall_times)
{
   // Initialize parameters
   parameters.setup_PT_parameters();
   parameters.setup();

   // Get lₕ, lᵥ, K_a
   K_a = parameters.K_air;
   const double lv = parameters.lv;
   const double lh = parameters.lh;

   // Import ω from the eigen-frequency stage.
   const double omega = parameters.read_omega();

   const double rho_air = parameters.rho_air;
   const double C_p_air = parameters.C_p_air;

   // Retrieve γ, α, compute k
   const double gamma = parameters.gamma;
   const double alpha = parameters.alpha;
   const double k = omega / parameters.c;

   // Initialize constants for temperature-pressure equations 
   a1 = k * k * (gamma - (lv/lh) * (gamma-1));
   a2 = k * k * gamma * alpha * (lv/lh - 1);
   a3 = k * k * gamma * (alpha / omega) * (lv/lh) / (rho_air * C_p_air);
   xi = PetscScalar (1, -gamma * k * lv);

   b2 = omega * rho_air * C_p_air;
   b1 = b2 * (gamma - 1) / (gamma * alpha);
   
   // PML coefficients
   pml_a = 2 * (2*parameters.w + parameters.g);
   pml_b = 8e-3;
   
   // Constants for the heat equation in the TF domain
   c1 = omega * parameters.rho_quartz * parameters.C_p_quartz;

   pcout << "Laser center: " << parameters.laser_center << endl;
   pcout << "Relative height: " << parameters.height_z_index << "/20" << endl;
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void ThermoAcoustic<dim> :: refine_child(const unsigned int level,
                   const typename DoFHandler<dim>::cell_iterator &cell,
                   const Point<dim> &current_cell)
{
   // Case 1: we have arrived at the correct cell.
   if (level == 0 &&
       cell -> is_locally_owned() &&
       (cell->center() - current_cell).norm_square() < 1e-16)
   {
      cell -> set_refine_flag();
   }
   // Case 2: we need to go deeper.
   if (level > 0 && cell->has_children())
      for (unsigned int q = 0; q < pow(2, dim); ++q)
         refine_child(level-1, cell->child(q), current_cell);
}

template <int dim>
void ThermoAcoustic<dim> :: import_refinements()
{
   // Number of refinement levels, including the coarsest. Also, number of
   // coarse cells that have been refined.
   std::ifstream import_param("../../EigenProblem/refinement/data.dat");
   unsigned int n_levels, n_cells;
   import_param >> n_levels >> n_cells;
   import_param.close();

   /*          Stage 1: read in refinement data         */
   std::ifstream import_ref("../../EigenProblem/refinement/refinements.dat");
   std::string readLine;

   // We have 'n_cells' coarse cells that have been refined. We begin by
   // associating the center of each coarse cell with the levels and centers of
   // its refined children.
   std::vector<std::set<std::string> > refinements(n_cells);
   std::vector<Point<dim> > cell_center(n_cells);

   for (unsigned int i = 0; i < n_cells; ++i)
   {
      // Read a point and store into the cell_center array.
      getline(import_ref, readLine);
      std::stringstream(readLine) >> cell_center[i][0]
                                  >> cell_center[i][1]
                                  >> cell_center[i][2];

      // Import adaptivity for this cell. If the line is empty, then there are
      // no more refinements to be read for this cell.
      getline(import_ref, readLine);
      while (import_ref.good() && readLine.length() != 0)
      {
         refinements[i].insert(readLine);
         getline(import_ref, readLine);
      }
   }
   import_ref.close();

   /*          Stage 2: assign refinements to triangulation         */
   const double g = parameters.g;
   Point<dim> laser_center = parameters.laser_center;
   // Convert point association to pointer association. Perform 1 level of
   // refinement.
   std::vector<typename Triangulation<dim>::cell_iterator> cell_ptr(n_cells);
   for (auto cell: dof_handler.cell_iterators())
   {
      if (cell -> material_id() == tf_id)
      {
         const Point<dim> current_center = cell -> center();

         // Cycle through all the refined cells in the previous module; possibly
         // no such cell will be found b/c it has never been refined.
         // Note: this algorithm should be optimized using K-D tree algorithms,
         // but at the moment this is quite fast.
         for (unsigned int i = 0; i < n_cells; ++i)
            if ((current_center-cell_center[i]).norm_square() < 1e-16)
            {
               cell_ptr[i] = cell;
               if (cell -> is_locally_owned())
                  cell -> set_refine_flag();
               break;
            }
      }
      else if (cell -> is_locally_owned() && cell -> material_id() == comp_id)
      {
         const Tensor<1, dim> pt = cell->center() - laser_center;
         if (pt[2]*pt[2] + pt[0]*pt[0] < pow(g/2 + l_mesh, 2))
            cell -> set_refine_flag();
      }
   }
   triangulation.execute_coarsening_and_refinement();

   // Perform one more refinement near the laser.
   for (auto cell: dof_handler.active_cell_iterators())
   {
      if (cell -> is_locally_owned() && cell -> material_id() == comp_id)
      {
         const Tensor<1, dim> pt = cell->center() - laser_center;
         if (pt[2]*pt[2] + pt[0]*pt[0] < pow(g/2 + l_mesh, 2))
            cell -> set_refine_flag();
      }
   }

   // Import deeper-level refinements (>= level 2).
   // Note: the code immediately above will not be executed if there is only
   // one level of refinement.
   for (unsigned int i = 1; i < n_levels-1; ++i)
   {
      for (unsigned int j = 0; j < n_cells; ++j)
      {
         for (auto v : refinements[j])
         {
            std::stringstream line(v);
            unsigned int level;
            line >> level;
            if (level == i)
            {
               Point<dim> center;
               line >> center[0] >> center[1] >> center[2];
               for (unsigned int q = 0; q < pow(2, dim); ++q)
               {
                  typename DoFHandler<dim> ::cell_iterator
                     cell(&triangulation, cell_ptr[j] -> level(),
                     cell_ptr[j] -> index(), &dof_handler);
                  if (cell->has_children())
                     refine_child(level-1, cell -> child(q), center);
               }
            }
         }
      }
      triangulation.execute_coarsening_and_refinement();
   }
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void ThermoAcoustic<dim> :: import_grid()
{
   GridIn<dim> import_mesh;
   import_mesh.attach_triangulation(triangulation);
   std::ifstream mesh_input ("../mesh.msh");
   import_mesh.read_msh(mesh_input);

   // Set material id for comp & PML cells
   for (auto cell: dof_handler.active_cell_iterators())
   {
      const Point<dim> cell_center = cell -> center();
      if ((cell_center(0) < 0) || (cell_center(0) > pml_a) ||
          (cell_center(2) < 0) || (cell_center(2) > pml_b))
      {
         cell -> set_material_id(pml_id);
      }
   }

   import_refinements();

   // Refine in the computational domain + immediately adjacent cells
   PetscInt n_comp_ref = 1;
   CHKERRXX(PetscOptionsGetInt(NULL, NULL, "-nPreRefinements", &n_comp_ref, NULL));

   for (PetscInt count = 0; count < n_comp_ref; ++count)
   {
      for (auto cell: dof_handler.active_cell_iterators())
      {
         const Point<dim> cell_center = cell -> center();
         // We slightly invade into the PML region in order to ensure a
         // reasonably smooth solution across the computational-PML interface
         if ((cell_center(0) > -1.0*l_mesh) &&
             (cell_center(0) < pml_a + 1.0*l_mesh) &&
             (cell_center(2) > -1.0*l_mesh) &&
             (cell_center(2) < pml_b + 1.0*l_mesh))
         {
            cell -> set_refine_flag();
         }
      }
      triangulation.execute_coarsening_and_refinement();
   }

   // Add refinements on the interface between QTF-air, near the tuning fork.
   const double g = parameters.g;
   PetscInt n_interface_ref = 0;
   CHKERRXX(PetscOptionsGetInt(NULL, NULL, "-nInterfaceRefinements",
            &n_interface_ref, NULL));

   for (PetscInt count = 0; count < n_interface_ref; ++count)
   {
      for (auto cell: dof_handler.active_cell_iterators())
      {
         const Tensor<1, dim> pt = cell->center() - parameters.laser_center;
         if (pt[2]*pt[2] + pt[0]*pt[0] < 2 * pow(g/2 + l_mesh, 2))
         {
            for (unsigned int f = 0; f < GeometryInfo<dim>::faces_per_cell; ++f)
            {
               if ((cell -> at_boundary(f) == false) &&
                   (cell -> material_id() != cell -> neighbor(f) -> material_id()))
               {
                  cell -> set_refine_flag();
               }
            }
         }
      }
      triangulation.execute_coarsening_and_refinement();
   }
   
   // Ensure that the cells across the air-QTF interface are on the same
   // refinemenet level. May require multiple passes to work.
   for (unsigned int i = 0; i < 2; ++i)
   {
      for (auto cell : dof_handler.active_cell_iterators())
      {
         if (cell -> is_locally_owned())
         {
            for (unsigned int f = 0; f < GeometryInfo<dim>::faces_per_cell; ++f)
            {
               if ((cell -> at_boundary(f) == false) &&
                   (cell -> neighbor(f) -> level() == cell -> level()) &&
                   (cell -> neighbor(f) -> has_children() == true) &&
                   (cell -> material_id() != cell -> neighbor(f) ->
                                                     material_id()))
               {
                  cell -> set_refine_flag();
               }
            }
         }
      }
      triangulation.execute_coarsening_and_refinement();
   }
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void ThermoAcoustic<dim> :: setup_system()
{
   // Allocate and determine the index sets of locally owned/relevant DoFs.
   dof_handler.distribute_dofs(fe);

   locally_owned_dofs = dof_handler.locally_owned_dofs();
   DoFTools :: extract_locally_relevant_dofs(dof_handler, locally_relevant_dofs);
   pcout << "# of complex DoFs: " << dof_handler.n_dofs() << endl;

   // Determine index sets for the subdomains
   index_sets();

   // RHS initialization. Solution vectors u_ will be initialized after we solve
   // the linear system.
   u.reinit(locally_owned_dofs, locally_relevant_dofs, MPI_COMM_WORLD);
   b.reinit(locally_owned_dofs, MPI_COMM_WORLD);

   // Sparsity pattern, matrix initialization.
   initialize_sparsity(hanging_nodes, dsp);
   A.reinit(locally_owned_dofs, locally_owned_dofs, dsp, MPI_COMM_WORLD);
   MassM.reinit(locally_owned_dofs, locally_owned_dofs, dsp, MPI_COMM_WORLD);

   initialize_sparsity(hanging_nodes_comp, dsp_comp);
   P_comp.reinit(locally_owned_dofs, locally_owned_dofs, dsp_comp, MPI_COMM_WORLD);
   Mass_comp.reinit(locally_owned_dofs, locally_owned_dofs, dsp_comp, MPI_COMM_WORLD);
}


// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void ThermoAcoustic<dim> :: index_sets()
{
   // We generate the index sets for the unknowns in each subdomain. Steps:
   // 1) Create distributed vectors that will contain nonzero entries for the
   //      DoFs that belong to a particular subdomain.
   PETScWrappers::MPI::Vector comp_dofs, TF_dofs;
   comp_dofs.reinit(locally_owned_dofs, MPI_COMM_WORLD);
   TF_dofs.reinit(locally_owned_dofs, MPI_COMM_WORLD);

   // 2) Cycle through all cells in the relevant domain, and set the value
   //    of all the locally *active* DoFs on the cell to a value ≥1.
   std::vector<types::global_dof_index> local_dof_indices (fe.dofs_per_cell);
   for (const auto &cell: dof_handler.active_cell_iterators())
      if (cell -> is_locally_owned())
      {
         cell -> get_dof_indices(local_dof_indices);
         if (cell -> material_id() == comp_id)
            for (unsigned int i = 0; i < fe.dofs_per_cell; ++i)
               comp_dofs[local_dof_indices[i]] += 1.0;
         else if (cell -> material_id() == tf_id)
            for (unsigned int i = 0; i < fe.dofs_per_cell; ++i)
               TF_dofs[local_dof_indices[i]] += 1.0;
      }

   // 3) Since locally active DoFs may belong to other processors, we exchange
   //    information between processors by adding the values up for each DoF.
   comp_dofs.compress(VectorOperation::add);
   TF_dofs.compress(VectorOperation::add);

   // We also extract the index sets for pressure.
   ComponentMask P = fe.component_mask(FEValuesExtractors::Scalar(0));
   IndexSet P_index = DoFTools::extract_dofs(dof_handler, P);

   // Notify the constraint matrix of hanging nodes.
   hanging_nodes.clear();
   DoFTools::make_hanging_node_constraints(dof_handler, hanging_nodes);
   hanging_nodes_comp.clear();
   DoFTools::make_hanging_node_constraints(dof_handler, hanging_nodes_comp);

   // For the preconditioner, we only need DoFs in the computational domain. Also, set
   // pressure to zero in the interior of the TF.
   for (auto i : locally_owned_dofs)
      if (comp_dofs(i) == 0)
      {
         hanging_nodes_comp.add_line(i);
         if (TF_dofs(i) != 0.0 && P_index.is_element(i))
            hanging_nodes.add_line(i);
      }

   // We now apply boundary conditions.
   VectorTools::interpolate_boundary_values(dof_handler,
               pml_face_id, ZeroFunction<dim, PetscScalar> (2), hanging_nodes);
   VectorTools::interpolate_boundary_values(dof_handler,
               pml_face_id, ZeroFunction<dim, PetscScalar> (2), hanging_nodes_comp);

   hanging_nodes.close();
   hanging_nodes_comp.close();

   // Finally, we establish the IndexSets for each locally_owned domain. The
   // results are stored into a std::vector<PetscInt> and later converted into
   // a PETSc IS (IndexSet) object for the solve() step.
   // First, we count the number of dofs stored on each processor.
   unsigned int n_comp_dofs = 0,       // Pressure, comp
                n_pml_dofs = 0,        // Pressure, PML
                n_pressure_dofs = 0,   // Entire pressure block
                n_temp_dofs = 0,       // Entire temperature block
                n_TF_dofs = 0;         // Temperature, TF block

   fs_compIndices.resize(locally_owned_dofs.n_elements());
   pr_comp_Indices.resize(locally_owned_dofs.n_elements());
   fs_pmlIndices.resize(locally_owned_dofs.n_elements());
   pr_pml_Indices.resize(locally_owned_dofs.n_elements());
   pressure_Indices.resize(locally_owned_dofs.n_elements());
   temp_Indices.resize(locally_owned_dofs.n_elements());
   TF_Indices.resize(locally_owned_dofs.n_elements());

   FE_Q<dim> fe_linear (1);
   DoFHandler<dim> dof_handler_Ponly;
   dof_handler_Ponly.reinit(triangulation);
   dof_handler_Ponly.distribute_dofs(fe_linear);
   const types::global_dof_index p_fieldsplit_start
      = dof_handler_Ponly.locally_owned_dofs().nth_index_in_set(0);

   for (auto i : locally_owned_dofs)
   {
      if (P_index.is_element(i))
      {
         // We are dealing with a DoF for pressure.
         if (comp_dofs(i) != 0.0)
         {
            pr_comp_Indices[n_comp_dofs] = i;
            fs_compIndices[n_comp_dofs] = p_fieldsplit_start + n_pressure_dofs;
            ++n_comp_dofs;
         }
         else if (TF_dofs(i) == 0.0)
         {
            pr_pml_Indices[n_pml_dofs] = i;
            fs_pmlIndices[n_pml_dofs] = p_fieldsplit_start + n_pressure_dofs;
            ++n_pml_dofs;
         }
         pressure_Indices[n_pressure_dofs] = i;
         ++n_pressure_dofs;
      }
      else
      {
         // This DoF is for temperature.
         if ((comp_dofs(i) != 0.0) || (TF_dofs(i) == 0.0))
         {
            temp_Indices[n_temp_dofs] = i;
            ++n_temp_dofs;
         }
         if (TF_dofs(i) != 0.0)
         {
            TF_Indices[n_TF_dofs] = i;
            ++n_TF_dofs;
         }
      }
   }

   // Eliminate unnecessary zeros at the end of the arrays.
   fs_compIndices.resize(n_comp_dofs);
   fs_pmlIndices.resize(n_pml_dofs);

   pr_comp_Indices.resize(n_comp_dofs);
   pr_pml_Indices.resize(n_pml_dofs);

   pressure_Indices.resize(n_pressure_dofs);
   temp_Indices.resize(n_temp_dofs);
   TF_Indices.resize(n_TF_dofs);

   // Output stats for each subdomain
   int global_pml_count, local_pml_count = n_pml_dofs,
       global_comp_count, local_comp_count = n_comp_dofs,
       global_pressure_count, local_pressure_count = n_pressure_dofs,
       global_temp_count, local_temp_count = n_temp_dofs,
       global_tf_count, local_tf_count = n_TF_dofs;
   MPI_Reduce(&local_pml_count, &global_pml_count, 1,
              MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
   MPI_Reduce(&local_comp_count, &global_comp_count, 1,
              MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
   MPI_Reduce(&local_pressure_count, &global_pressure_count, 1,
              MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
   MPI_Reduce(&local_temp_count, &global_temp_count, 1,
              MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
   MPI_Reduce(&local_tf_count, &global_tf_count, 1,
              MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
   const int temp_count = dof_handler.n_dofs() - global_pressure_count;

   pcout << "Problem dimensions:" << endl
         << " Domain\t\tDoF count\t     %" << endl
         << "-----------------------------------------------" << endl
         << "Temp\t\t  " << temp_count << "  \t"
         << (double) temp_count / dof_handler.n_dofs() * 100 << "%" << endl
         << "   T : gas\t  " << global_temp_count << endl
         << "   T : QTF\t  " << global_tf_count << endl
         << "   T : shared\t  "
         << global_temp_count+global_tf_count - temp_count << endl
         << "Pressure\t  " << global_pressure_count << "  \t"
         << (double) global_pressure_count / dof_handler.n_dofs() * 100 << "%" << endl
         << "   P : comp\t  " << global_comp_count << endl
         << "   P : PML\t  " << global_pml_count << endl
         << "   P : ignored\t  "
         << global_pressure_count-global_comp_count-global_pml_count << endl
         << "-----------------------------------------------" << endl;
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void ThermoAcoustic<dim> :: assemble_system()
{
   // Quadrature, basis function values for interior assembly
   QGauss<dim> quadrature_formula (3);
   QGauss<dim-1> face_quadrature_formula (3);

   FEValues<dim> fe_values (fe, quadrature_formula,
                            update_values | update_gradients |
                            update_quadrature_points | update_JxW_values);
   FEFaceValues<dim> fe_face_values (fe, face_quadrature_formula,
                                     update_values | update_quadrature_points |
                                     update_JxW_values);

   const unsigned int dofs_per_cell = fe.dofs_per_cell,
                      n_quad = quadrature_formula.size(),
                      n_face_quad = face_quadrature_formula.size(),
                      n_cell_faces = GeometryInfo<dim>::faces_per_cell;
   
   std::vector<types::global_dof_index> local_dof_indices (dofs_per_cell);
   
   FullMatrix<PetscScalar> cell_A (dofs_per_cell, dofs_per_cell);
   FullMatrix<PetscScalar> cell_Mass (dofs_per_cell, dofs_per_cell);
   Vector<PetscScalar> cell_b (dofs_per_cell);

   f<dim, PetscScalar> RHS (parameters, a3);
   PML_coeff<dim> pml_coeff (pml_a, pml_b, pml_thick, coeff);
   std::vector<Vector<PetscScalar> > RHS_vals (n_quad, Vector<PetscScalar> (2));

   Tensor<2, dim, PetscScalar> B = Tensor<2, dim, PetscScalar> ();
   
   // Extract components of a vector
   const FEValuesExtractors::Scalar P (0);
   const FEValuesExtractors::Scalar T (1);

   for (const auto &cell : dof_handler.active_cell_iterators())
   {
      if (cell -> is_locally_owned())
      {
         if (cell -> material_id() == tf_id)
         {
            fe_values.reinit(cell);
            cell_A = PetscScalar (0.0);

            for (unsigned int i = 0; i < dofs_per_cell; ++i)
               for (unsigned int j = 0; j < dofs_per_cell; ++j)
                  for (unsigned int q = 0; q < n_quad; ++q)
                     cell_A(i, j) += (
                              -  parameters.K_tf *
                                 fe_values[T].gradient(i, q) *
                                 fe_values[T].gradient(j, q)
                              +  PETSC_i * c1 *
                                 fe_values[T].value(i, q) *
                                 fe_values[T].value(j, q))
                              *
                                 fe_values.JxW(q);

            cell -> get_dof_indices(local_dof_indices);
            hanging_nodes.distribute_local_to_global(cell_A, 
                                                     local_dof_indices,
                                                     A);
            hanging_nodes_comp.distribute_local_to_global(cell_A,
                                                          local_dof_indices,
                                                          P_comp);
         }
         else  // Computational domain or PML region
         {
            fe_values.reinit(cell);
            cell_A = PetscScalar (0.0);
            cell_Mass = PetscScalar (0.0);
            cell_b = PetscScalar (0.0);
            
            RHS.vector_value_list(fe_values.get_quadrature_points(),
                                  RHS_vals);

            for (unsigned int q = 0; q < n_quad; ++q)
            {
               const Point<dim> pt = fe_values.quadrature_point(q);
               const PetscScalar beta_x(1, pml_coeff.value_x(pt));
               const PetscScalar beta_z(1, pml_coeff.value_z(pt));
               const PetscScalar beta_xz = beta_x*beta_z;

               // B(x)
               B[0][0] = beta_z/beta_x;
               B[1][1] = beta_z*beta_x;
               B[2][2] = beta_x/beta_z;

               for (unsigned int i = 0; i < dofs_per_cell; i++)
               {
                  for (unsigned int j = 0; j < dofs_per_cell; j++)
                  {
                     // Note: subscript i corresponds to a test function,
                     //                 j corresdonds to a trial function.
                     cell_Mass(i, j) +=
                                 fe_values[P].value(i, q) *
                                 fe_values[P].value(j, q) *
                                 fe_values.JxW(q);

                     cell_A(i, j) += (
                              // Row 1
                              -  xi * B *
                                 fe_values[P].gradient(i, q) *
                                 fe_values[P].gradient(j, q)
                              +  a1 * beta_xz * 
                                 fe_values[P].value(i, q) *
                                 fe_values[P].value(j, q)
                              +  a2 * beta_xz *
                                 fe_values[P].value(i, q) *
                                 fe_values[T].value(j, q)
                              // Row 2
                              -  K_a * B *
                                 fe_values[T].gradient(i, q) *
                                 fe_values[T].gradient(j, q)
                              +  PETSC_i * b2 * beta_xz *
                                 fe_values[T].value(i, q) *
                                 fe_values[T].value(j, q)
                              -  PETSC_i * b1 * beta_xz *
                                 fe_values[T].value(i, q) *
                                 fe_values[P].value(j, q))
                              *
                                 fe_values.JxW(q);
                  }
                  
                  cell_b(i) += (fe_values[P].value(i, q) *
                                 RHS_vals[q](0)
                              +  fe_values[T].value(i, q) *
                                 RHS_vals[q](1))
                              *
                                 fe_values.JxW(q);
               }
            }

            cell -> get_dof_indices(local_dof_indices);
            hanging_nodes.distribute_local_to_global(cell_A, cell_b,
                                                     local_dof_indices,
                                                     A, b);
            hanging_nodes.distribute_local_to_global(cell_Mass,
                                                     local_dof_indices,
                                                     MassM);

            // Assembly of the custom preconditioner for P block.
            if (cell -> material_id() == comp_id)
            {
               for (unsigned int face_id = 0; face_id < n_cell_faces; ++face_id)
               {
                  if ((cell -> face(face_id) -> at_boundary() == false) &&
                      (cell -> neighbor(face_id) -> material_id() == pml_id))
                  {
                     fe_face_values.reinit(cell, face_id);
                     for (unsigned int q = 0; q < n_face_quad; ++q)
                        for (unsigned int i = 0; i < dofs_per_cell; ++i)
                           for (unsigned int j = 0; j < dofs_per_cell; ++j)
                              cell_A(i, j) +=
                              +  PETSC_i * sqrt(a1) *
                                 fe_face_values[P].value(i, q) *
                                 fe_face_values[P].value(j, q) *
                                 fe_face_values.JxW(q);
                  }
               }
               hanging_nodes_comp.distribute_local_to_global(cell_A,
                                                             local_dof_indices,
                                                             P_comp);
               hanging_nodes_comp.distribute_local_to_global(cell_Mass,
                                                             local_dof_indices,
                                                             Mass_comp);
            }
         }
      }
   }
   
   A.compress(VectorOperation::add);
   P_comp.compress(VectorOperation::add);

   MassM.compress(VectorOperation::add);
   Mass_comp.compress(VectorOperation::add);

   b.compress(VectorOperation::add);
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void ThermoAcoustic<dim> :: solve()
{
   timer.enter_subsection("Setup Solver");

   // Data structure that contains all the PETSc objects
   PetscData *data = new PetscData;

   // Set up index sets for subdomains
   IS pressureIS, temperatureIS, TFtemperatureIS;
   IS_Convert(pressure_Indices, pressureIS);
   IS_Convert(temp_Indices, temperatureIS);
   IS_Convert(fs_compIndices, data->fs_comp_IS);
   IS_Convert(fs_pmlIndices, data->fs_pml_IS);
   IS_Convert(pr_comp_Indices, data->pr_comp_IS);
   IS_Convert(pr_pml_Indices, data->pr_pml_IS);
   IS_Convert(TF_Indices, TFtemperatureIS);

   data->Pcomp = P_comp;
   data->MassComp = Mass_comp;
   data->MassM = MassM;

   // Obtain restricted versions of the submatrices
   CHKERRXX(MatCreateSubMatrix(Mass_comp, data->pr_comp_IS, data->pr_comp_IS,
                            MAT_INITIAL_MATRIX, &data->MassComp_r));
   CHKERRXX(MatCreateSubMatrix(MassM, pressureIS, pressureIS,
                            MAT_INITIAL_MATRIX, &data->MassM_r));

   CHKERRXX(MatCreateSubMatrix(P_comp, data->pr_comp_IS, data->pr_comp_IS,
                            MAT_INITIAL_MATRIX, &data->Pcomp_r));

   CHKERRXX(MatCreateSubMatrix(A, data->pr_pml_IS, data->pr_pml_IS,
                            MAT_INITIAL_MATRIX, &data->Ppml_r));
   CHKERRXX(MatCreateSubMatrix(A, data->pr_pml_IS, data->pr_comp_IS,
                            MAT_INITIAL_MATRIX, &data->A21_r));

   // Initialize objects for the DD aproach. In particular, we may need two
   // copies of the same vector for the different domains. We also need to be
   // able to restrict global vectors/matrices to rows that correspond to a
   // particular domain.
   MatCreateVecs(data->MassM_r, &temp, NULL);
   VecDuplicate(temp, &rhs);

   // Set up outer preconditioner
   PetscInt n_iter = 25;
   KSP ksp, *subksp;
   PC pc;
   KSPCreate(MPI_COMM_WORLD, &ksp);
   KSPSetType(ksp, "fgmres");
   KSPSetOperators(ksp, A, A);
   KSPSetTolerances(ksp, 1e-10, 1e-20, PETSC_DEFAULT, n_iter);
   CHKERRXX(KSPSetFromOptions(ksp));
   KSPSetResidualHistory(ksp, NULL, n_iter, PETSC_FALSE);

   KSPGetPC(ksp, &pc);
   CHKERRXX(PCSetType(pc, "fieldsplit"));
   PCFieldSplitSetBlockSize(pc, 3);
   CHKERRXX(PCFieldSplitSetIS(pc, "T", temperatureIS));
   CHKERRXX(PCFieldSplitSetIS(pc, "TF", TFtemperatureIS));
   CHKERRXX(PCFieldSplitSetIS(pc, "P", pressureIS));

   CHKERRXX(KSPSetUp(ksp));
   CHKERRXX(PCFieldSplitGetSubKSP(pc, NULL, &subksp));

   // Temp sub-block
   KSPSetType(subksp[0], "fgmres");
   KSPSetTolerances(subksp[0], 1e-6, 1e-15, PETSC_DEFAULT, 200);
   KSPMonitorCancel(subksp[0]);
   KSPSetFromOptions(subksp[0]);

   // TF block
   KSPMonitorCancel(subksp[1]);
   KSPSetFromOptions(subksp[1]);

   // Pressure sub-block
   KSPSetType(subksp[2], "fgmres");
   KSPSetTolerances(subksp[2], 1e-4, 1e-15, PETSC_DEFAULT, 10);
   PC pressurePC;
   KSPGetPC(subksp[2], &pressurePC);
   PCSetType(pressurePC, "shell");
   PCShellSetContext(pressurePC, data);
   PCShellSetApply(pressurePC, PressureBlock);
   SetUpPressureBlock(data);

   // Option to show residuals for inner iterations.
   PetscBool inner_residuals = PETSC_FALSE;
   CHKERRXX(PetscOptionsGetBool(NULL, NULL, "-show_inner_residuals",
                                &inner_residuals, NULL));
   if (inner_residuals)
   {
      KSPMonitorSet(subksp[2], MonitorPressureBlock, NULL, NULL);
      KSPMonitorSet(subksp[0], MonitorTempBlock, NULL, NULL);
      KSPMonitorSet(subksp[1], MonitorTFBlock, NULL, NULL);
      KSPMonitorSet(data->ksp_comp, MonitorPressureComp, NULL, NULL);
   }

   timer.leave_subsection();
   timer.enter_subsection("Solve");

   // Initialize solution vector u
   PETScWrappers::MPI::Vector u_distributed;
   u_distributed.reinit(locally_owned_dofs, MPI_COMM_WORLD);

   CHKERRXX(KSPSolve(ksp, b, u_distributed));

   hanging_nodes.distribute(u_distributed);
   u = u_distributed;
   
   Vec z;
   MatCreateVecs(A, &z, NULL);
   MatMult(A, u, z);
   VecAXPY(z, -1.0, b);
   
   PetscReal norm, norm_b;
   VecNorm(z, NORM_2, &norm);
   VecNorm(b, NORM_2, &norm_b);
   pcout << "Norm: " << norm/norm_b << endl;

   // Output residual history
   const PetscReal *residuals;
   PetscInt n_actual_iter;
   CHKERRXX(KSPGetResidualHistory(ksp, &residuals, &n_actual_iter));
   pcout << "\nPrinting (global) residual history" << endl;
   for (PetscInt i = 0; i < n_actual_iter; ++i)
      pcout << i << "\t" << residuals[i] << endl;

   // Destroy PETSc objects
   CHKERRXX(KSPDestroy(&ksp));
   petsc_destroy(data);
   CHKERRXX(ISDestroy(&pressureIS));
   CHKERRXX(ISDestroy(&temperatureIS));
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void ThermoAcoustic<dim> :: refine_grid()
{
   unsigned int n_cell_faces = GeometryInfo<dim>::faces_per_cell;

   Vector<float> estimated_error_per_cell (triangulation.n_active_cells());
   KellyErrorEstimator<dim>::estimate(dof_handler,
                       QGauss<dim-1> (2),
                       std::map<types::boundary_id, const Function<dim, PetscScalar>*>(),
                       u,
                       estimated_error_per_cell);

   const double g = parameters.g;
   unsigned int cell_index = 0;
   for (auto cell : dof_handler.active_cell_iterators())
   {
      if (cell -> is_locally_owned())
      {
         // No refinements in the PML region or the QTF
         if (cell -> material_id() != comp_id)
         {
            estimated_error_per_cell(cell_index) = 0;
         }
         // Also, do not refine too far away from the laser
         const Tensor<1, dim> pt = cell->center() - parameters.laser_center;
         if (pt[2]*pt[2] + pt[0]*pt[0] > 2.5 * pow(g/2 + l_mesh, 2))
         {
            estimated_error_per_cell(cell_index) = 0;
         }
      }
      ++cell_index;
   }

   parallel::distributed::GridRefinement::
      refine_and_coarsen_fixed_number (triangulation,
                                       estimated_error_per_cell,
                                       0.1, 0.0);
   triangulation.execute_coarsening_and_refinement();

   // Ensure that the cells across the air-QTF interface are on the same
   // refinemenet level.
   for (auto cell : dof_handler.active_cell_iterators())
   {
      if (cell -> is_locally_owned())
      {
         for (unsigned int f = 0; f < n_cell_faces; ++f)
         {
            if ((cell -> at_boundary(f) == false) &&
                (cell -> neighbor(f) -> level() == cell -> level()) &&
                (cell -> neighbor(f) -> has_children() == true) &&
                (cell -> material_id() != cell -> neighbor(f) ->
                                                  material_id()))
            {
               cell -> set_refine_flag();
            }
         }
      }
   }
   triangulation.execute_coarsening_and_refinement();
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void ThermoAcoustic<dim> :: export_solution()
{
   /* Export solution. Export_TF only exports the solution on the QTF. This part
      is for visualization only, so either choice is fine.
   */
   // Export_TF<dim> data_out (tf_id);
   DataOut<dim> data_out;
   data_out.attach_dof_handler(dof_handler);

   std::vector<std::string> solution_names(1, "pressure");
   solution_names.push_back("temperature");

   data_out.add_data_vector(u, solution_names);
   data_out.build_patches();

   std::string filename = "../output/solution_proc_"
                        + Utilities::int_to_string(rank, 3)
                        + ".vtu";
   
   std::ofstream outputVTU(filename.c_str());
   data_out.write_vtu(outputVTU);

   if (rank == 0)
   {
      std::vector<std::string> filenames;
      for (unsigned int i = 0; i < world_size; ++i)
      {
         filenames.push_back("solution_proc_"
                           + Utilities::int_to_string(i, 3)
                           + ".vtu");
      }

      std::string main_filename = "../output/w_solution.pvtu";
      std::ofstream outputPTVU(main_filename);
      data_out.write_pvtu_record(outputPTVU, filenames);
   }

   /* Export solution on the QTF for the deformation module. First, we reduce
      the size of the output by coarsening the solution to remove any additional
      refinements that we do globally.
   */
   PetscInt n_comp_ref = 0;
   CHKERRXX(PetscOptionsGetInt(NULL, NULL, "-nPreRefinements",
            &n_comp_ref, NULL));

   if (n_comp_ref > 0)
   {
      // Interpolates solution from one mesh to a solution on a coarser mesh.
      parallel::distributed::SolutionTransfer
         <dim, PETScWrappers::MPI::Vector> transfer(dof_handler);

      // We cannot interpolate across multiple mesh coarsenings, so we will have
      // to do this level-by-level.
      for (PetscInt i = 0; i < n_comp_ref; ++i)
      {
         for (auto cell: dof_handler.active_cell_iterators())
            if (cell -> is_locally_owned() && (cell -> level() > 0))
               cell -> set_coarsen_flag();

         triangulation.prepare_coarsening_and_refinement();
         transfer.prepare_for_coarsening_and_refinement(u);
         triangulation.execute_coarsening_and_refinement();

         // We need to repartition DoFs and update the index sets.
         dof_handler.distribute_dofs(fe);
         locally_owned_dofs = dof_handler.locally_owned_dofs();
         DoFTools :: extract_locally_relevant_dofs(dof_handler,
                                                   locally_relevant_dofs);
      }
   }

   /* Now that we have coarsened the solution, we will export it by cycling
      through the support points of each cell and storing the solution values
      into a std::map object.
      We do not store the values directly because the algorithm produces a large
      number of duplicates, which the std::map object eliminates.
   */
   std::vector<Point<dim> > pts = fe.get_unit_support_points();
   Quadrature<dim> support_pts (pts);
   std::vector<Vector<PetscScalar> > value(support_pts.size(), Vector<PetscScalar> (2));
   FEValues<dim> fe_values (fe, support_pts,
                            update_values | update_quadrature_points);
	std::map<Point<dim>, std::array<PetscScalar, 2>, KDtreeComparison> PTvalues;

   for (auto cell: dof_handler.active_cell_iterators())
   {
      if (cell -> is_locally_owned() && (cell -> material_id() == tf_id))
      {
         fe_values.reinit(cell);
         fe_values.get_function_values(u, value);
         
         for (unsigned int i = 0; i < support_pts.size(); ++i)
         {
            auto &values = PTvalues[fe_values.quadrature_point(i)];
            values[0] = value[i][0];
            values[1] = value[i][1];
         }
      }
   }

   // At this point, we are ready to export the solution.
   filename = "../TFsolution/solution_QTF_"
            + Utilities::int_to_string(rank, 3)
            + ".txt";
   std::ofstream outputTF(filename.c_str());
   outputTF.precision(10);

   for (auto point_in_map : PTvalues)
   {
      outputTF << point_in_map.first;
      for (unsigned int i = 0; i < 2; ++i)
         outputTF << " " << point_in_map.second[i].real()
                  << " " << point_in_map.second[i].imag();
      outputTF << endl;
   }
   outputTF.close();

   int n_global_tf_dofs, n_local_tf_dofs = PTvalues.size();
   MPI_Reduce(&n_local_tf_dofs, &n_global_tf_dofs, 1,
              MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

   if (rank == 0)
   {
      // Output certain stats for the QTF deformation module.
      std::ofstream output("../TFsolution/Ndofs.dat");
      output << world_size << endl << n_global_tf_dofs;
      output.close();
   }
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void ThermoAcoustic<dim> :: run()
{
   timer.enter_subsection("Import Grid");
   import_grid();
   timer.leave_subsection();

   for (unsigned int refinement_step = 0; refinement_step < 1;
        ++refinement_step)
   {
      if (refinement_step > 0)
      {
         timer.print_summary();
         timer.reset();
         
         pcout << "Refinement step " << refinement_step << ": " << endl;
         timer.enter_subsection("Refine mesh");
         refine_grid();
         timer.leave_subsection();
      }
      timer.enter_subsection("Setup System");
      setup_system();   
      timer.leave_subsection();
         
      timer.enter_subsection("Assemble System");
      assemble_system();
      pcout << "System Assembled" << endl << endl;
      timer.leave_subsection();

      solve();
      pcout << "Solved" << endl;
   }
   timer.enter_subsection("Export Solution");

   export_solution();
   timer.leave_subsection();
}

