/* Function to export the solution on the QTF only. All we do here is overwrite
   the first_cell() and the next_cell() functions to skip over all non-QTF
   cells.
*/

#include <deal.II/numerics/data_out.h>

using namespace dealii;

template <int dim, class DofH = DoFHandler<dim> >
class Export_TF : public DataOut<dim, DoFHandler<dim> >
{
public:
	typedef typename DataOut_DoFData<DofH, dim, dim>
	   :: cell_iterator cell_iterator;

	Export_TF(const unsigned int tf_id_arg) : DataOut<dim, DoFHandler<dim> > (),
	              tf_id(tf_id_arg) {}
	typename DataOut<dim, DofH> :: cell_iterator first_cell();
	typename DataOut<dim, DofH> :: 
	                 cell_iterator next_cell(const cell_iterator &current_cell);

private:
   const unsigned int tf_id;
};

template <int dim, class DofH>
typename DataOut<dim, DofH> :: cell_iterator Export_TF<dim, DofH>
   :: first_cell()
{
	typename Triangulation<dim, dim> :: active_cell_iterator
	      cell = this -> triangulation -> begin_active();
	while (cell -> material_id() != tf_id)
	{
		++cell;
	}
	return cell;
}

template <int dim, class DofH>
typename DataOut<dim, DofH> :: cell_iterator Export_TF<dim, DofH>
   :: next_cell(const cell_iterator &current_cell)
{
	typename Triangulation<dim, dim>
	   :: active_cell_iterator cell = current_cell;
	++cell;
	while ((cell != this -> triangulation -> end()) &&
	       (cell -> material_id() != tf_id))
	{
		++cell;
	}
	return cell;
}
