/* RHS of system; in form [ ia₃S]
                          [ -b₃S],
   but with laser beam that is of constant width w.r.t. y-direction:
   S = (A / (w₀)²)exp[-2((x-x₀)² + (z-z₀)²) / (w₀)²].
   
   Note: Number = complex<double>
*/

using namespace dealii;

template <int dim, typename Number>
class f_constant : public Function<dim, Number>
{
public:
	f_constant(Parameters &parameters, const double a3_arg);
	virtual void vector_value(const Point<dim> &p,
							        Vector<Number> &values) const;
	virtual void vector_value_list(const std::vector<Point<dim> > &points,
                             std::vector<Vector<Number> > &value_list) const;

private:
	double a3, A, w0;
	
   // Since the thickness of the laser varies, the source term depends on its
   // distance along the laser axis.
	double x_laser, z_laser;
};

template <int dim, typename Number>
f_constant <dim, Number> :: f_constant(Parameters& parameters,
                                       const double a3_arg)
   : Function<dim, Number> (2)
{
	a3 = a3_arg;

	A = (parameters.alpha_eff * parameters.W_L) / (numbers::PI);
	w0 = parameters.w_0;
   
   x_laser = parameters.laser_center(0);
   z_laser = parameters.laser_center(2);
}

template <int dim, typename Number>
void f_constant<dim, Number> :: vector_value(const Point <dim> &p,
					               		         Vector <Number> &values) const
{
   const double w_sqd = w0 * w0;
	const double S = A / w_sqd /
	                 exp(2*(pow(p(0)-x_laser,2) + pow(p(2)-z_laser,2))/w_sqd);

	values(0) = Number (0, a3 * S);
	values(1) = Number (-S, 0);
}

template <int dim, typename Number>
void f_constant<dim, Number> ::
   vector_value_list(const std::vector<Point<dim> > &points,
			            std::vector<Vector<Number> > &value_list) const
{
	for (unsigned int k = 0; k < points.size(); k++)
		f_constant<dim, Number> :: vector_value(points[k], value_list[k]);
}
