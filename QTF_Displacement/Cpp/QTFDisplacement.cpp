// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
QTFDisplacement<dim> :: ~QTFDisplacement()
{
   dof_handler.clear();
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
QTFDisplacement<dim> :: QTFDisplacement() :
   triangulation(MPI_COMM_WORLD),
   fe(FE_Q<dim> (FE_order), dim),
   fe_PT(FE_Q<dim> (FE_order), 4),
   dof_handler(triangulation),
   dof_handler_PT(triangulation),
   rank(Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)),
   world_size(Utilities::MPI::n_mpi_processes(MPI_COMM_WORLD)),
   pcout(std::cout, rank == 0),
   timer(MPI_COMM_WORLD, pcout, TimerOutput::summary, TimerOutput::wall_times)
{
   parameters.import_Q();
   parameters.setup();
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void QTFDisplacement<dim> :: refine_child(const unsigned int level,
                   const typename DoFHandler<dim>::cell_iterator &cell,
                   const Point<dim> &current_cell)
{
   // Case 1: we have arrived at the correct cell.
   if (level == 0 && (cell->center() - current_cell).norm_square() < 1e-16)
      cell -> set_refine_flag();

   // Case 2: we need to go deeper.
   if (level > 0 && cell->has_children())
      for (unsigned int q = 0; q < pow(2, dim); ++q)
         refine_child(level-1, cell->child(q), current_cell);
}

template <int dim>
void QTFDisplacement<dim> :: import_refinements()
{
   // Number of refinement levels, including the coarsest. Also, number of cells
   // that have been refined.
   std::ifstream import_param("../../EigenProblem/refinement/data.dat");
   unsigned int n_levels, n_cells;
   import_param >> n_levels >> n_cells;
   import_param.close();

   // Import refinements
   std::ifstream import_ref("../../EigenProblem/refinement/refinements.dat");
   std::string readLine;

   // We have 'n_cells' coarse cells that have been refined. We begin by
   // associating the center of each coarse cell with the levels and centers of
   // its refined children.
   std::vector<std::set<std::string> > refinements(n_cells);
   std::vector<Point<dim> > cell_center(n_cells);

   for (unsigned int i = 0; i < n_cells; ++i)
   {
      // Store the points into the cell_center array.
      getline(import_ref, readLine);
      std::stringstream(readLine) >> cell_center[i][0]
                                  >> cell_center[i][1]
                                  >> cell_center[i][2];

      // Import adaptivity for this cell. If the line is empty, then there are
      // no more refinements to be read for this cell.
      getline(import_ref, readLine);
      while (import_ref.good() && readLine.length() != 0)
      {
         refinements[i].insert(readLine);
         getline(import_ref, readLine);
      }
   }
   import_ref.close();

  // Convert point association to pointer association. Perform 1 level of
   // refinement.
   std::vector<typename Triangulation<dim>::cell_iterator> cell_ptr(n_cells);
   for (auto cell: dof_handler.cell_iterators())
   {
      const Point<dim> current_center = cell -> center();
      
      // Cycle through all the refined cells in the previous module; possibly
      // no such cell will be found b/c it has never been refined.
      for (unsigned int i = 0; i < n_cells; ++i)
         if ((current_center-cell_center[i]).norm_square() < 1e-16)
         {
            cell_ptr[i] = cell;
            cell -> set_refine_flag();
            break;
         }
   }
   triangulation.execute_coarsening_and_refinement();

   // Import deeper-level refinements (>= level 2).
   for (unsigned int i = 1; i < n_levels-1; ++i)
   {
      for (unsigned int j = 0; j < n_cells; ++j)
      {
         for (auto v : refinements[j])
         {
            std::stringstream line(v);
            unsigned int level;
            line >> level;
            if (level == i)
            {
               Point<dim> center;
               line >> center[0] >> center[1] >> center[2];
               for (unsigned int q = 0; q < pow(2, dim); ++q)
               {
                  typename DoFHandler<dim> ::cell_iterator
                           cell(&triangulation, cell_ptr[j] -> level(),
                           cell_ptr[j] -> index(), &dof_handler);
                  if (cell->has_children())
                     refine_child(level-1, cell -> child(q), center);
               }
            }
         }
      }
      triangulation.execute_coarsening_and_refinement();
   }
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void QTFDisplacement<dim> :: import_grid()
{
   // Import full mesh that includes both air and the QTF,
   Triangulation<dim> air_and_qtf_mesh;
   GridIn<dim> import_mesh;
   import_mesh.attach_triangulation(air_and_qtf_mesh);
   std::ifstream mesh_input ("../../PT_Eqns/mesh.msh");
   import_mesh.read_msh(mesh_input);
   
   // then extract the QTF submesh.
   const unsigned int tf_id = 1;
   std::set<typename Triangulation<dim>::active_cell_iterator> cells_to_remove;
   for (auto cell : air_and_qtf_mesh.active_cell_iterators())
      if (cell -> material_id() != tf_id)
         cells_to_remove.insert(cell);
   GridGenerator::create_triangulation_with_removed_cells(air_and_qtf_mesh,
                                                          cells_to_remove,
                                                          triangulation);

   const double g = parameters.g;
   const double cylinder_middle_x = 2*parameters.w + g,
                cylinder_middle_y = parameters.hp + g/2;

   // Also label boundaries (bottom, y=0 face and the curved semicylinder).
   for (const auto &cell : dof_handler.active_cell_iterators())
   {
      for (unsigned int face_id = 0;
           face_id < GeometryInfo<dim>::faces_per_cell; ++face_id)
      {
         if (cell -> face(face_id) -> at_boundary())
         {
            const Point<dim> face_center = cell -> face(face_id) -> center();
            if (face_center[2] < 1.0e-14)
               cell -> face(face_id) -> set_boundary_id(QTF_bottom_id);
            else if (face_center[1] < 1.0e-14)
               cell -> face(face_id) -> set_boundary_id(QTF_symmetrical_face);
            else
            {
               const double dist_sq = pow(face_center[0]-cylinder_middle_x, 2) +
                                      pow(face_center[1]-cylinder_middle_y, 2);
               if (std::abs(dist_sq - g*g/4) < 1.0e-14)
                  cell -> face(face_id) -> set_boundary_id(QTF_curved_boundary);
            }
         }
      }
   }

   // Import adaptivity from the eigenvalue module.
   import_refinements();
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void QTFDisplacement<dim> :: setup_system()
{
   // Allocate and determine the index sets of locally owned/relevant DoFs.
   dof_handler.distribute_dofs(fe);

   locally_owned_dofs = dof_handler.locally_owned_dofs();
   DoFTools :: extract_locally_relevant_dofs(dof_handler, locally_relevant_dofs);

   pcout << "# of complex DoFs: " << dof_handler.n_dofs() << endl;

   hanging_nodes.clear();
   DoFTools::make_hanging_node_constraints(dof_handler, hanging_nodes);

   // Set the bottom of the QTF to be motionless.
   VectorTools::interpolate_boundary_values(dof_handler,
                                            QTF_bottom_id,
                                            ZeroFunction<dim, PetscScalar> (dim),
                                            hanging_nodes);
   // We can also assume that the vibrations will be symmetrical across
   // y=y_c (y-component of the center of laser). In that case, there will be no
   // motion in the y direction on this face.
	const std::vector<bool> select_Y = {false, true, false};
	VectorTools::interpolate_boundary_values(dof_handler,
				                                QTF_symmetrical_face,
				                                ZeroFunction<dim, PetscScalar> (dim),
				                                hanging_nodes,
				                                fe.component_mask(select_Y));
   hanging_nodes.close();

   // Import the P-T solution from the PT module.
   dof_handler_PT.reinit(triangulation);
   dof_handler_PT.distribute_dofs(fe_PT);
   
   PT.reinit(dof_handler_PT.n_dofs());
   PTsolution<dim, double> PT_function(triangulation);
   VectorTools::interpolate(MappingQGeneric<dim> (4), dof_handler_PT, PT_function, PT);

   // Sparsity pattern, matrix initialization.
   DynamicSparsityPattern dsp (locally_relevant_dofs);
   DoFTools::make_sparsity_pattern(dof_handler, dsp, hanging_nodes, false);
   SparsityTools::distribute_sparsity_pattern(dsp,
                                              locally_owned_dofs,
                                              MPI_COMM_WORLD,
                                              locally_relevant_dofs);

   A.reinit(locally_owned_dofs, locally_owned_dofs, dsp, MPI_COMM_WORLD);
   u.reinit(locally_owned_dofs, locally_relevant_dofs, MPI_COMM_WORLD);
   b_pres.reinit(locally_owned_dofs, MPI_COMM_WORLD);
   b_temp.reinit(locally_owned_dofs, MPI_COMM_WORLD);
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void QTFDisplacement<dim> :: assemble_system()
{
   const double omega = parameters.read_omega(),
                delta_s = omega / parameters.Q;
   const PetscScalar constC = parameters.rho_quartz * (omega*omega +
                                                       PETSC_i*delta_s*omega);

   const unsigned int n_cell_faces = GeometryInfo<dim>::faces_per_cell;

   // Quadrature, basis function values for interior assembly
   QGauss<dim> quadrature_formula (FE_order+1);
   FEValues<dim> fe_values (fe, quadrature_formula,
                            update_values | update_gradients |
                            update_quadrature_points | update_JxW_values);
   FEValues<dim> fe_values_PT (fe_PT, quadrature_formula,
                               update_values | update_quadrature_points);

   QGauss<dim-1> face_quadrature_formula (FE_order+1);
   FEFaceValues<dim> fe_face_values (fe, face_quadrature_formula,
                                     update_values | update_quadrature_points |
                                     update_normal_vectors | update_JxW_values);
   FEFaceValues<dim> fe_face_values_PT (fe_PT, face_quadrature_formula,
                                     update_values | update_quadrature_points);

   const unsigned int dofs_per_cell = fe.dofs_per_cell,
                      n_quad = quadrature_formula.size(),
                      n_face_quad = face_quadrature_formula.size();

   std::vector<types::global_dof_index> local_dof_indices (dofs_per_cell);

   FullMatrix<PetscScalar> cell_A (dofs_per_cell, dofs_per_cell);
   Vector<PetscScalar> cell_b_pres (dofs_per_cell),
                       cell_b_temp (dofs_per_cell);

   const FEValuesExtractors::Vector U (0);
   const FEValuesExtractors::Scalar P (0);
   const FEValuesExtractors::Scalar T (1);

   for (const auto &cell : dof_handler.active_cell_iterators())
   {
      if (cell -> is_locally_owned())
      {
         // Create a cell object for the DoF Handler for the PT solutions.
         // Otherwise will crash in debug mode. It's the same cell however.
         typename DoFHandler<dim> ::cell_iterator
                     cell_PT(&triangulation, cell -> level(),
                             cell -> index(), &dof_handler_PT);

         fe_values.reinit(cell);
         fe_values_PT.reinit(cell_PT);
         cell_A = PetscScalar (0.0);
         cell_b_pres = PetscScalar (0.0);
         cell_b_temp = PetscScalar (0.0);

         std::vector<Vector<double> > T_value(n_quad, Vector<double> (4));
         fe_values_PT.get_function_values(PT, T_value);

         for (unsigned int q = 0; q < n_quad; ++q)
         {
            for (unsigned int i = 0; i < dofs_per_cell; ++i)
            {
               // i - test function, j - trial function
               const SymmetricTensor<2, dim>
                     grad_V = fe_values[U].symmetric_gradient(i, q);
               for (unsigned int j = 0; j < dofs_per_cell; ++j)
               {
                  cell_A(i, j) += (
                           constC *
                           fe_values[U].value(i, q) *
                           fe_values[U].value(j, q)
                        -
                           parameters.elasticity_T *
                           fe_values[U].symmetric_gradient(j, q) *
                           grad_V) *
                           fe_values.JxW(q);
               }

               cell_b_temp(i) +=
                        +  PetscScalar(T_value[q][2], T_value[q][3]) *
                          (parameters.elasticity_T *
                           parameters.alpha_T *
                           grad_V) *
                           fe_values.JxW(q);
            }
         }

         for (unsigned int face_id = 0; face_id < n_cell_faces; ++face_id)
         {
            if (cell -> face(face_id) -> at_boundary() == true)
            {
               fe_face_values.reinit(cell, face_id);
               fe_face_values_PT.reinit(cell_PT, face_id);

               std::vector<Vector<double> > P_value(n_face_quad,
                                                    Vector<double> (4));
               fe_face_values_PT.get_function_values(PT, P_value);

               for (unsigned int i = 0; i < dofs_per_cell; ++i)
               {
                  for (unsigned int q = 0; q < n_face_quad; ++q)
                  {
                     cell_b_pres(i) += 
                        -  PetscScalar(P_value[q][0], P_value[q][1]) *
                           fe_face_values.normal_vector(q) *
                           fe_face_values[U].value(i, q) *
                           fe_face_values.JxW(q);
                  }
               }
            }
         }
         cell -> get_dof_indices(local_dof_indices);
         hanging_nodes.distribute_local_to_global(cell_A, cell_b_pres,
                                                  local_dof_indices,
                                                  A, b_pres);
         hanging_nodes.distribute_local_to_global(cell_b_temp,
                                                  local_dof_indices,
                                                  b_temp);
      }
   }

   A.compress(VectorOperation::add);
   b_temp.compress(VectorOperation::add);
   b_pres.compress(VectorOperation::add);
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void QTFDisplacement<dim> :: solve()
{
   timer.enter_subsection("Solve");

   KSP ksp;
   KSPCreate(MPI_COMM_WORLD, &ksp);
   KSPSetOperators(ksp, A, A);
   KSPSetTolerances(ksp, 1e-8, 1e-50, PETSC_DEFAULT, 100);
   KSPSetFromOptions(ksp);

   // Initialize solution vector u
   PETScWrappers::MPI::Vector u_pres_distributed, u_temp_distributed;
   u_pres_distributed.reinit(locally_owned_dofs, MPI_COMM_WORLD);
   u_temp_distributed.reinit(locally_owned_dofs, MPI_COMM_WORLD);

   // Solve the system
   CHKERRXX(KSPSolve(ksp, b_pres, u_pres_distributed));
   CHKERRXX(KSPSolve(ksp, b_temp, u_temp_distributed));

   double local_max_displacement_pres = 0, local_max_displacement_temp = 0;
   PetscScalar local_displacement_pres_v = 0, local_displacement_temp_v = 0;
   PetscScalar value_p, value_t;
   for (PetscInt i : locally_owned_dofs)
   {
      VecGetValues(u_pres_distributed, 1, &i, &value_p);
      VecGetValues(u_temp_distributed, 1, &i, &value_t);

      const double amp_p = abs(value_p);
      if (amp_p > local_max_displacement_pres)
      {
         local_max_displacement_pres = amp_p;
         local_displacement_pres_v = value_p;
      }
      const double amp_t = abs(value_t);
      if (amp_t > local_max_displacement_temp)
      {
         local_max_displacement_temp = amp_t;
         local_displacement_temp_v = value_t;
      }
   }

   PetscScalar *rank_max_p, *rank_max_t;
   rank_max_p = (PetscScalar*) malloc(world_size*sizeof(PetscScalar));
   rank_max_t = (PetscScalar*) malloc(world_size*sizeof(PetscScalar));
   MPI_Gather(&local_displacement_pres_v, 1, MPI_DOUBLE_COMPLEX,
              rank_max_p, 1, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
   MPI_Gather(&local_displacement_temp_v, 1, MPI_DOUBLE_COMPLEX,
              rank_max_t, 1, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);

   if (rank == 0)
   {
      PetscScalar max_pres = 0, max_temp = 0;
      double max_pres_amp = 0, max_temp_amp = 0;
      for (unsigned int i = 0; i < world_size; ++i)
      {
         const double amp_p = abs(rank_max_p[i]);
         if (amp_p > max_pres_amp)
         {
            max_pres_amp = amp_p;
            max_pres = rank_max_p[i];
         }
         const double amp_t = abs(rank_max_t[i]);
         if (amp_t > max_temp_amp)
         {
            max_temp_amp = amp_t;
            max_temp = rank_max_t[i];
         }
      }
      
      double multiplier = 2 * parameters.read_omega() * 7.0e-6;
      pcout << "P: " << (std::complex<double>) max_pres  << endl
            << "amp: " << max_pres_amp << endl
            << "T: " << (std::complex<double>) max_temp << endl
            << "amp: " << max_temp_amp << endl
            << "P+T signal (pA) : "
            << abs(max_pres + max_temp) * multiplier * 1.0e+12 << endl
            << "P-T signal (pA) : "
            << abs(max_pres - max_temp) * multiplier * 1.0e+12 << endl;
   }

   u_pres_distributed += u_temp_distributed;
   hanging_nodes.distribute(u_pres_distributed);
   u = u_pres_distributed;
}

// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void QTFDisplacement<dim> :: export_solution()
{
   std::vector<DataComponentInterpretation::DataComponentInterpretation>
      data_component_interpretation(dim,
                  DataComponentInterpretation::component_is_part_of_vector);

   DataOut<dim> data_out;
   data_out.attach_dof_handler(dof_handler);
   data_out.add_data_vector(u, "u",
                               DataOut<dim>::type_dof_data,
                               data_component_interpretation);
   data_out.build_patches();
   
   std::string filename = "../output/solution_proc_"
                   + Utilities::int_to_string(rank, 2)
                   + ".vtu";
   
   std::ofstream output(filename.c_str());
   data_out.write_vtu(output);
   
   if (rank == 0)
   {
      std::vector<std::string> filenames;
      for (unsigned int i = 0; i < world_size; ++i)
      {
         filenames.push_back("solution_proc_"
                           + Utilities::int_to_string(i, 2)
                           + ".vtu");
      }

      std::string main_filename = "../output/w_solution.pvtu";
      std::ofstream full_output(main_filename);
      data_out.write_pvtu_record(full_output, filenames);
   }
}


// ██████████████████████████████████████████████████████████████████████████ //
template <int dim>
void QTFDisplacement<dim> :: run()
{
   timer.enter_subsection("Import Grid");
   import_grid();
   timer.leave_subsection();

   timer.enter_subsection("Setup System");
   setup_system();   
   timer.leave_subsection();

   timer.enter_subsection("Assemble System");
   assemble_system();
   pcout << "System Assembled" << endl << endl;
   timer.leave_subsection();

   solve();
   pcout << "Solved" << endl;

   timer.enter_subsection("Export Solution");
   export_solution();
   timer.leave_subsection();
}
