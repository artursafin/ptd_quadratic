#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/utilities.h>
#include <deal.II/base/index_set.h>
#include <deal.II/base/timer.h>

#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/affine_constraints.h>
#include <deal.II/lac/sparsity_tools.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>

#include <deal.II/grid/grid_in.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/grid_generator.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_q.h>

#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/error_estimator.h>

#include <deal.II/distributed/shared_tria.h>
#include <deal.II/distributed/grid_refinement.h>

#include <deal.II/lac/petsc_sparse_matrix.h>
#include <deal.II/lac/petsc_vector.h>

#include <cmath>
#include <fstream>

#include "../../PT_Eqns/Cpp/Parameters.cpp"
#include "PTsolution.cpp"

using namespace dealii;
using std::endl;

// ██████████████████████████████████████████████████████████████████████████ //

template <int dim>
class QTFDisplacement
{
public:
   QTFDisplacement();
   ~QTFDisplacement();
   void run();

private:
   void import_grid();
   
   void import_refinements();
   void import_PTsolution();

   void setup_system();
   void assemble_system();
   void solve();
   void export_solution();

   void refine_child(const unsigned int level,
                     const typename DoFHandler<dim>::cell_iterator &cell,
                     const Point<dim> &current_cell);

   parallel::shared::Triangulation<dim> triangulation;
   FESystem<dim> fe, fe_PT;
   DoFHandler<dim> dof_handler, dof_handler_PT;

   Parameters parameters;           // Constants for the model.

   AffineConstraints<PetscScalar> hanging_nodes;  // For handling adaptive refinements
   PETScWrappers::MPI::SparseMatrix A;
   PETScWrappers::MPI::Vector u, b_temp, b_pres;
   Vector<double> PT;
   
   // Index sets that contain DoFs owned by a processor, and locally active DoFs
   IndexSet locally_relevant_dofs, locally_owned_dofs;

   // Current processor rank, number of processors
   const unsigned int rank, world_size;

   const unsigned int QTF_bottom_id = 1,
                      QTF_symmetrical_face = 2,
                      QTF_curved_boundary = 3;

   ConditionalOStream pcout;        // Displays output *only* from rank 0
   TimerOutput timer;               // Wall clock time

   // Order of the FE polynomial space
   static const unsigned int FE_order = 2;
};

#include "QTFDisplacement.cpp"
