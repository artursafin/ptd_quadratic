/* Class that imports the PT-solutions and provides their values on their
   support points
*/

using namespace dealii;

struct KDtreeComparison
{
   // Comparison logic: point A is < than point B if A_z < B_z. If equal, then
   // A < B if A_x < B_x. If equal, then A < B if A_y < B_y. Since no point is
   // repeated, equality cannot be achieved in the last comparison.
   // This is identical to KD-tree sorting.
   bool operator()(const Point<3> &a, const Point<3> &b) const
   {
      if (a[2] < b[2])
         return true;
      else if (a[2] > b[2])
         return false;
      else
      {
         if (a[0] < b[0])
            return true;
         else if (a[0] > b[0])
            return false;
         else
         {
            return (a[1] < b[1]);
         }
      }
   }
};

template <int dim, typename Number>
class PTsolution : public Function<dim, Number>
{
public:
	PTsolution(const parallel::shared::Triangulation<dim> &triangulation);
	virtual void vector_value(const Point<dim> &p,
							        Vector<Number> &values) const;
	virtual void vector_value_list(const std::vector<Point<dim> > &points,
                             std::vector<Vector<Number> > &value_list) const;

private:
   parallel::shared::Triangulation<dim> refined_triangulation;
   FE_Q<dim> fe_linear;
   DoFHandler<dim> dof_handler_linear;

	std::map<Point<dim>, std::array<double, 4>, KDtreeComparison> PTvalues;
};

template <int dim, typename Number>
PTsolution<dim, Number> :: PTsolution(const parallel::shared::
                                      Triangulation<dim> &triangulation) :
   Function<dim, Number> (4),
   refined_triangulation(MPI_COMM_WORLD),
   fe_linear(1),
   dof_handler_linear(refined_triangulation)
{
   // Create a triangulation that supports the linear elements from the previous
   // module, by performing an extra global refinement.
   refined_triangulation.copy_triangulation(triangulation);
   refined_triangulation.refine_global();

   // Initialize spaces, etc.
   dof_handler_linear.distribute_dofs(fe_linear);
   
   // Import the solutions
   // Import number of dofs from the PT-system.
	std::ifstream read_ndofs("../../PT_Eqns/TFsolution/Ndofs.dat");
	if(!read_ndofs.is_open())
		throw std::invalid_argument("Could not open Ndofs.dat");

   unsigned int n_files;
	read_ndofs >> n_files;
	read_ndofs.close();

   Point<dim> support_pt;
   for (unsigned int i = 0; i < n_files; ++i)
   {
      std::ifstream read_solution("../../PT_Eqns/TFsolution/solution_QTF_" +
                                  Utilities::int_to_string(i, 3) + ".txt");
	   if(!read_solution.is_open())
		   throw std::invalid_argument("2D BC's file has failed to open");

      std::string temp;
      getline(read_solution, temp);

      while (temp.length() != 0)
      {
         std::stringstream temp_str (temp);
         temp_str >> support_pt[0] >> support_pt[1] >> support_pt[2];
         
         auto &values = PTvalues[support_pt];
         for (unsigned int ct = 0; ct < 4; ++ct)
            temp_str >> values[ct];

         getline(read_solution, temp);
      }
   }
}

template <int dim, typename Number>
void PTsolution<dim, Number> :: vector_value(const Point <dim> &p,
					      		                  Vector <Number> &values) const
{
   // Compute the lower point pointer. Here's a pointer: don't point that
   // pointer to the point.
   auto lower_pt = PTvalues.upper_bound(Point<dim> (p[0], p[1], p[2] + 1e-8));

   if (lower_pt == PTvalues.end())
      --lower_pt;

   bool found = false;
   while (!found)
   {
      if ((p - lower_pt->first).norm_square() < 1e-16)
      {
         for (unsigned int i = 0; i < 4; ++i)
            values[i] = lower_pt->second[i];
         found = true;
         break;
      }
      else
      {
         if (lower_pt == PTvalues.begin())
         {
            std::cout << "Error: cannot find point " << p << std::endl;
            throw std::invalid_argument("Aborting");
            break;
         }
         --lower_pt;
      }
   }
}

template <int dim, typename Number>
void PTsolution<dim, Number> :: vector_value_list(
                           const std::vector<Point<dim> > &points,
					            std::vector<Vector<Number> > &value_list) const
{
	for (unsigned int k = 0; k < points.size(); k++)
		PTsolution<dim, Number> :: vector_value(points[k], value_list[k]);
}
